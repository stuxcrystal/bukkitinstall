/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall;

import java.util.Locale;

/**
 * An object that receives messages.
 *
 * @author StuxCrystal
 */
public interface MessageReceiver {

    /**
     * Send a message to the receiver.
     *
     * @param message The message to send.
     */
    public void sendMessage(String... message);

    /**
     * Checks if the sender has permissions to do the job.<p />
     *
     * Use this to implement PermissionChecks on Migrators.
     *
     * @param node The permission-node to check.
     * @return true if the receiver has the permission node otherwise false.
     */
    public boolean hasPermission(String node);

    /**
     * Returns the locale for the MessageReceiver.
     * @return a locale.
     */
    public Locale getLocale();

}
