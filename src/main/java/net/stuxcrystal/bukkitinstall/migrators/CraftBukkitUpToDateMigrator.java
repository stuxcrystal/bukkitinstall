/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.migrators;

import de.nofear13.craftbukkituptodate.CraftBukkitUpToDateHelper;
import net.stuxcrystal.bukkitinstall.BukkitInstallPlugin;
import net.stuxcrystal.bukkitinstall.MessageReceiver;
import net.stuxcrystal.bukkitinstall.api.Migrator;
import net.stuxcrystal.bukkitinstall.api.PluginList;
import net.stuxcrystal.bukkitinstall.utils.PatternArrayNameFilter;
import org.bukkit.ChatColor;
import org.bukkit.plugin.PluginDescriptionFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import static net.stuxcrystal.bukkitinstall.l10n.Translations._;

/**
 * Migrates the updates downloaded from CraftBukkitUpToDate into the plugin-folder.
 */
public class CraftBukkitUpToDateMigrator implements Migrator {

    private BukkitInstallPlugin install;

    public CraftBukkitUpToDateMigrator(BukkitInstallPlugin install) {
        this.install = install;
    }

    @Override
    public String getName() {
        return install.getPluginList().getPlugin("CraftBukkitUpToDate")==null?null:"cbutd";
    }

    @Override
    public void execute(MessageReceiver sender, String[] arguments) throws IOException {
        PluginList list = install.getPluginList();
        CraftBukkitUpToDateHelper helper = CraftBukkitUpToDateHelper.getInstance();
        File update = new File(helper.getDownloadFolder(), "plugins");

        File[] files = update.listFiles(new PatternArrayNameFilter(install.getPluginLoader().getPluginFileFilters()));

        if (files.length == 0) {
            sender.sendMessage(_(sender, "migrations.cbutd.nothing"));
            return;
        }

        for (File file : files) {
            PluginDescriptionFile pdf = list.getDescription(file);
            File orig = list.getPluginFile(pdf.getName());

            if (orig == null)
                orig = new File(list.getPluginFolder(), file.getName());

            sender.sendMessage(_(sender, "migrations.cbutd.copy", pdf.getName()));
            Files.copy(file.toPath(), orig.toPath(), StandardCopyOption.REPLACE_EXISTING);
        };

        for (File file :files) {
            PluginDescriptionFile pdf = list.getDescription(file);
            File orig = list.getPluginFile(pdf.getName());

            if (orig == null)
                orig = new File(list.getPluginFolder(), file.getName());

            sender.sendMessage(_(sender, "migrations.cbutd.remove", pdf.getName()));
            file.delete();
        };
    }
}
