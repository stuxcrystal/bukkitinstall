/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.databases;

import net.stuxcrystal.bukkitinstall.api.databases.PluginDatabase;
import net.stuxcrystal.bukkitinstall.api.databases.PluginDescription;
import net.stuxcrystal.bukkitinstall.tasks.RequestSiteTask;
import net.stuxcrystal.bukkitinstall.utils.IOUtil;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * Installs plugins using BukGet.<p />
 *
 * BukGet is an API for DevBukkit that scraches data from DevBukkit.
 */
public class BukGetDatabase implements PluginDatabase {

    /**
     * Format String for BukGet.
     */
    public static final String URL_FORMAT = "http://api.bukget.org/1/plugin/%s/%s/";

    /**
     * The URL requested when searching...
     */
    public static final String SEARCH_FORMAT = "http://api.bukget.org/3/search";

    /**
     * The name of the PluginDatabase will be BukGet instead of DevBukkit.<p />
     *
     * We're accessing BukGet not DevBukkit directly.
     * @return
     */
    @Override
    public String getName() {
        return "BukGet";
    }

    @Override
    public PluginDescription getPlugin(String name) throws IOException {
        return getPlugin(name, "latest");
    }

    @Override
    public PluginDescription getPlugin(String name, String version) throws IOException {
        // Download Info File...
        String data = new RequestSiteTask(getInformationURI(name, version)).run();
        JSONObject object = (JSONObject) JSONValue.parse(data);
        String link = IOUtil.getJSONValue(object, "versions", 0, "dl_link");
        String filename = IOUtil.getJSONValue(object, "versions", 0, "filename");
        String realName = IOUtil.getJSONValue(object, "plugin_name");
        String url = IOUtil.getJSONValue(object, "bukkitdev_link");

        // Sometimes the plugin_name value is empty.
        // Use this instead.
        if (realName.isEmpty())
            realName = IOUtil.getJSONValue(object, "name");

        List<String> authors = IOUtil.getJSONValue(object, "authors");
        String desc = IOUtil.getJSONValue(object, "desc");
        String ver = IOUtil.getJSONValue(object, "versions", 0, "version");

        return new PluginDescription(
            realName, desc, link, filename, version, url, authors
        );
    }

    @Override
    public List<PluginDescription> searchPlugins(String search) throws IOException {
        Map<String, String> filters = new HashMap<String, String>();
        filters.put("field", "plugin_name");
        filters.put("action", "like");
        filters.put("value", search);

        String string = JSONValue.toJSONString(Arrays.asList(filters));

        Map<String, String> post_data = new HashMap<String, String>();
        post_data.put("fields", "slug,description,authors,dbo_page,versions.version,versions.filename,versions.download");
        post_data.put("start", "" + 0);
        post_data.put("size", "" + 10);
        post_data.put("filters", string);

        String result = new RequestSiteTask(new URL(SEARCH_FORMAT), IOUtil.encodeQuery(post_data)).run();
        JSONArray object = (JSONArray) JSONValue.parse(result);
        List<PluginDescription> result_list = new ArrayList<PluginDescription>(object.size());

        for (Object o : object) {
            String name = IOUtil.getJSONValue(o, "slug");
            String desc = IOUtil.getJSONValue(o, "description");
            List<String> authors = IOUtil.getJSONValue(o, "authors");
            String website = IOUtil.getJSONValue(o, "dbo_page");
            String version = IOUtil.getJSONValue(o, "versions", 0, "version");
            String filename = IOUtil.getJSONValue(o, "versions", 0, "filename");
            String download = IOUtil.getJSONValue(o, "versions", 0, "download");

            result_list.add(new PluginDescription(
                name, desc, website, filename, version, download, authors
            ));
        }

        return result_list;
    }

    /**
     * Creates an URI from the Plugin name
     *
     * @param name
     * @return
     */
    public static URL getInformationURI(String name, String version) {
        try {
            return new URL(String.format(URL_FORMAT, name.toLowerCase(), version.toLowerCase()));
        } catch (MalformedURLException e) {
            return null;
        }
    }
}
