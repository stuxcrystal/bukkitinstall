/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.databases;

import net.stuxcrystal.bukkitinstall.api.BukkitInstall;
import net.stuxcrystal.bukkitinstall.api.databases.PluginDatabase;
import net.stuxcrystal.bukkitinstall.api.databases.PluginDescription;
import net.stuxcrystal.bukkitinstall.tasks.DownloadTask;
import org.apache.commons.lang.StringUtils;
import org.bukkit.plugin.PluginDescriptionFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Use a direct URI to download files.<p />
 *
 * This database does not allow ports.
 *
 * @author StuxCrystal
 */
public class URLDatabase implements PluginDatabase {

    /**
     * The cache of the plugin.
     */
    private class Cache {
        /**
         * The current file in the cache.
         */
        public File f;

        /**
         * The plugin description created by the download process.
         */
        public PluginDescription description;

        /**
         * The plugin description file extracted after the download process.
         */
        public PluginDescriptionFile pdf;

        /**
         * The time the item was added to the cache.
         */
        public long dlTime;

        /**
         * The link.
         */
        public String name;

        public Cache(File f, PluginDescription description, PluginDescriptionFile pdf, String name) {
            this.f = f;
            this.description = description;
            this.pdf = pdf;
            this.dlTime = System.currentTimeMillis();
        }

        public void remove() {
            this.f.deleteOnExit();
            this.f.delete();
        }

    }

    /**
     * A reference to BukkitInstall.
     */
    private BukkitInstall install;

    /**
     * The current cache.
     */
    private List<Cache> cache = new ArrayList<Cache>();


    public URLDatabase(BukkitInstall install) {
        this.install = install;
    }

    @Override
    public String getName() {
        return "URL";
    }

    @Override
    public PluginDescription getPlugin(String name) throws IOException {
        return getPlugin(name, null);
    }

    @Override
    public PluginDescription getPlugin(String name, String version) throws IOException {
        name = name + ":" + version;

        if (URL_REGEX.matcher(name) == null)
            throw new FileNotFoundException("This is not a valid URL.");

        return getCache(name).description;
    }

    /**
     * Is not supported by this database. :'(
     * @return null
     * @throws IOException
     */
    @Override
    public List<PluginDescription> searchPlugins(String search) {
        return null;
    }

    /**
     * This method is also clearing the cache.
     */
    private Cache getCache(String name) throws IOException {
        long cTime = System.currentTimeMillis();
        Cache cache = null;
        List<Cache> remove = new ArrayList<Cache>();
        synchronized (this.cache) {
            for (Cache item : this.cache) {
                if (item.dlTime >= cTime+(1000*60*60)) {
                    remove.add(item);
                    continue;
                }

                if (StringUtils.equals(item.name, name))
                    cache = item;
            }

            for (Cache item : remove) {
                this.cache.remove(item);
                item.remove();
            }
        }

        if (cache == null) {
            cache = dlItem(name);
            synchronized (this.cache) {
                this.cache.add(cache);
            }
        }

        return cache;
    }

    private Cache dlItem(String link) throws IOException {
        File temp = File.createTempFile("binst_idl_", ".tmp");
        URL url = new URL(link);
        DownloadTask task = new DownloadTask(temp, url);
        task.run();

        PluginDescriptionFile pdf = install.getPluginList().getDescription(temp);

        if (pdf == null)
            throw new IOException("This is not a valid plugin");

        temp.delete();

        PluginDescription description = new PluginDescription(
                pdf.getName(), pdf.getDescription(), link, url.getFile().replaceAll("(.*?)/", ""), pdf.getVersion(), link, pdf.getAuthors()
        );

        return new Cache(temp, description, pdf, link);
    }

    /**
     * Default Regular Expression for a URL.
     */
    private static final Pattern URL_REGEX = Pattern.compile(
        "(((ht|f)tp(s?)://)\\S+)"
    );


}
