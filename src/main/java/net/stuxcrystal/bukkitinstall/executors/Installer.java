/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.executors;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import net.stuxcrystal.bukkitinstall.BukkitInstallPlugin;
import net.stuxcrystal.bukkitinstall.FailedExecutionException;
import net.stuxcrystal.bukkitinstall.MessageReceiver;
import net.stuxcrystal.bukkitinstall.api.BukkitInstall;
import net.stuxcrystal.bukkitinstall.api.PluginList;
import net.stuxcrystal.bukkitinstall.api.PluginSearcher;
import net.stuxcrystal.bukkitinstall.api.databases.PluginDescription;
import net.stuxcrystal.bukkitinstall.l10n.Headers;
import net.stuxcrystal.bukkitinstall.tasks.DownloadTask;
import net.stuxcrystal.bukkitinstall.tasks.RequestSiteTask;
import net.stuxcrystal.bukkitinstall.utils.IOUtil;

import org.bukkit.ChatColor;
import org.bukkit.plugin.PluginDescriptionFile;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import static net.stuxcrystal.bukkitinstall.l10n.Translations._;

/**
 * Installer for Plugins.
 *
 * @author StuxCrystal
 */
public class Installer {

    /**
     * Stores data about Zip Files.
     *
     * @author StuxCrystal
     */
    private static class TempContentData {
        File path;

        File file;

        public TempContentData(File path, File file) {
            this.path = path;
            this.file = file;
        }
    }

    /**
     * The cache for the plugin downloads.
     */
    private Map<String, PluginDescription> cache = new HashMap<String, PluginDescription>();

    /**
     * All downloads where their dependencies were checked.
     */
    private Map<String, File> checked_downloads = new HashMap<String, File>();

    /**
     * The references to the unchecked plugins.
     */
    private Map<String, File> unchecked_downloads = new HashMap<String, File>();

    /**
     * The filenames of the jar files.
     */
    private Map<String, String> download_names = new HashMap<String, String>();

    /**
     * All plugins that are downloading zip files are stored here.
     */
    private Map<String, TempContentData[]> zipContents = new HashMap<String, TempContentData[]>();

    /**
     * Files to be deleted while cleaning up.
     */
    private List<File> toDelete = new ArrayList<File>();

    /**
     * Plugins to update...
     */
    private List<String> updatePlugins = new ArrayList<String>();

    /**
     * Output
     */
    private MessageReceiver sender;

    /**
     * True if soft-dependencies should be installed.
     */
    private boolean useSoftDepend = false;

    /**
     * True if the installer should update the plugin.
     */
    private boolean forceUpdate = false;

    /**
     * True if the installer should ignore plugins that are no plugins used by a sen
     */
    private boolean ignoreAdditionalPlugins = false;

    /**
     * List of *existing* plugins.
     */
    private PluginList list;

    /**
     * Handler to the databases registered in BukkitInstall.
     */
    private PluginSearcher searcher;

    /**
     * Prepares the installer.
     */
    public Installer(MessageReceiver sender, PluginList list, PluginSearcher searcher) {
        this.sender = sender;
        this.list = list;
        this.searcher = searcher;
    }

    /**
     * Checks if the plugin is known to the plugin.
     *
     * @param name
     * @return
     */
    public boolean isInstalled(String name) {
        return this.list.exists(name) || checked_downloads.containsKey(name) || unchecked_downloads.containsKey(name);
    }

    /**
     * Install the plugin.
     *
     * @param plugin_names
     */
    public void installPlugins(String[] plugin_names) {
        sender.sendMessage(Headers.getHeader(this.sender, _(this.sender, "installer.title")));
        sender.sendMessage(_(this.sender, "installer.prepare"));
        this.cleanup();

        sender.sendMessage(_(this.sender, "installer.check.description"));
        boolean hasUninstalls = false;
        List<String> names = new ArrayList<String>();
        for (String plugin : plugin_names) {
            if (plugin.startsWith("-")) {
                hasUninstalls = true;
                continue;
            }

            // Download the plugin-description.
            String[] data = this.parsePluginName(plugin);

            PluginDescription description = this.getDescription(data[1], data[2]);

            if (description == null) {
                sender.sendMessage(_(sender, "installer.download.notfound"));
                return;
            }

            plugin = description.getName();

            if (isInstalled(plugin)) {
                if (this.forceUpdate || data[0].contains("u")) {
                    names.add(plugin);
                    this.updatePlugins.add(this.list.getDescription(plugin).getName());
                } else {
                    sender.sendMessage(_(this.sender, "installer.check.installed", plugin));
                }
            } else {
                names.add(plugin);
            }
        }

        if (names.size() == 0) {
            sender.sendMessage(_(this.sender, "installer.check.none"));
            return;
        }

        sender.sendMessage(_(this.sender, "installer.download.description"));
        for (String plugin : names) {
            try {
                downloadPlugin(plugin);
            } catch (FileNotFoundException e) {
                sender.sendMessage(_(this.sender, "installer.download.notfound"));
                clearTemp();
                return;
            } catch (FailedExecutionException e) {
                sender.sendMessage(ChatColor.RED + e.getMessage());
                clearTemp();
                return;
            } catch (IOException e) {
                sender.sendMessage(_(this.sender, "installer.download.failed"));
                BukkitInstallPlugin.getInstance().getLogger().log(Level.SEVERE, "Failed to download: " + plugin, e);
                clearTemp();
                return;
            }
        }

        try {
            while (handleDependencies()) ;
        } catch (FileNotFoundException e) {
            sender.sendMessage(_(this.sender, "installer.download.notfound"));
            clearTemp();
            return;
        } catch (FailedExecutionException e) {
            sender.sendMessage(ChatColor.RED + e.getMessage());
            clearTemp();
            return;
        } catch (IOException e) {
            sender.sendMessage(_(this.sender, "installer.download.failed"));
            BukkitInstallPlugin.getInstance().getLogger().log(Level.SEVERE, "Dependency download failed.", e);
            clearTemp();
            return;
        }

        // I should comment my code better next time. :'(
        if (this.forceUpdate) {
            sender.sendMessage(_(this.sender, "installer.misc.update-retrieve.description"));
            for (String name : this.updatePlugins) {
                String plname = this.list.getPluginFile(name).getName();
                if (plname == null)
                    sender.sendMessage(_(this.sender, "installer.misc.update-retrieve.failed", name));
                this.download_names.put(name.toLowerCase(), plname);
            }
        }

        try {
            installTempDir();
        } catch (FailedExecutionException e) {
            sender.sendMessage(e.getMessage());
            clearTemp();
            return;
        } catch (IOException e) {
            sender.sendMessage(_(this.sender, "installer.install.failed"));
            BukkitInstallPlugin.getInstance().getLogger().log(Level.SEVERE, "Copy failed.", e);
            clearTemp();
            return;
        }

        clearTemp();

        if (hasUninstalls) {
            List<String> uninstall = new ArrayList<String>();
            for (String plugin : plugin_names) {
                if (plugin.startsWith("-")) {
                    uninstall.add(plugin.substring(1));
                }
            }

            sender.sendMessage(_(this.sender, "installer.uninstall.description"));
            Uninstaller uninstaller = new Uninstaller(sender, list);
            try {
                uninstaller.uninstallPlugins(uninstall.toArray(new String[0]));
            } catch (FailedExecutionException e) {
                sender.sendMessage(_(this.sender, "installer.uninstall.failed"));
                BukkitInstallPlugin.getInstance().getLogger().log(Level.SEVERE, "Uninstall.", e);
            }
        }
    }

    /**
     * Does some cleanup before starting the entire install thing...
     */
    private void cleanup() {
        this.cache.clear();
        this.updatePlugins.clear();
        this.checked_downloads.clear();
        this.unchecked_downloads.clear();
        this.zipContents.clear();
        this.download_names.clear();
        this.toDelete.clear();
    }

    /**
     * Downloads all dependencies
     *
     * @return
     * @throws java.io.IOException
     */
    private boolean handleDependencies() throws IOException {
        boolean hasNewDependency = false;

        HashMap<String, File> dl = new HashMap<String, File>(unchecked_downloads);
        unchecked_downloads.clear();

        for (Entry<String, File> plugin : dl.entrySet()) {
            PluginDescriptionFile desc = this.list.getDescription(plugin.getValue());
            if (desc == null) {
                throw new FailedExecutionException(_(this.sender, "installer.dependencies.nodescription", plugin.getKey()));
            }

            List<String> dependencies;

            if (!useSoftDepend) {
                dependencies = desc.getDepend();
                if (dependencies == null)
                    continue;
            } else {
                List<String> hard_dependencies = desc.getDepend();
                List<String> soft_dependencies = desc.getSoftDepend();
                dependencies = new ArrayList<String>();

                if (hard_dependencies != null)
                    dependencies.addAll(hard_dependencies);
                if (soft_dependencies != null)
                    dependencies.addAll(soft_dependencies);
            }

            for (String dependency : dependencies)
                if (!isInstalled(dependency.toLowerCase()) && !dl.containsKey(dependency.toLowerCase())) {
                    downloadPlugin(dependency);
                    hasNewDependency = true;
                }
        }

        checked_downloads.putAll(dl);

        return hasNewDependency;
    }

    /**
     * Copies temporary files to the plugin folder.
     *
     * @throws java.io.IOException
     */
    private void installTempDir() throws IOException {
        sender.sendMessage(_(this.sender, "installer.install.description"));
        // Assuming that the data-folder is there...
        File plugin_dir = this.list.getPluginFolder();

        for (Entry<String, File> entries : checked_downloads.entrySet())
            copyFile(entries.getValue(), new File(plugin_dir, download_names.get(entries.getKey())));

        for (Entry<String, TempContentData[]> entries : zipContents.entrySet())
            for (TempContentData file : entries.getValue())
                copyFile(file.file, new File(plugin_dir, file.path.getPath()));

    }

    /**
     * Copies a file.
     *
     * @param from
     * @param to
     * @throws java.io.IOException
     */
    @SuppressWarnings("resource")
    private void copyFile(File from, File to) throws IOException {
        // Make directories
        to.mkdirs();

        // Copy now...
        Files.copy(from.toPath(), to.toPath(), StandardCopyOption.REPLACE_EXISTING);
    }

    /**
     * Clears the Temporary files...
     */
    private void clearTemp() {
        sender.sendMessage(ChatColor.GREEN + "Cleaning up...");

        for (Entry<String, File> entries : checked_downloads.entrySet())
            entries.getValue().delete();

        for (Entry<String, File> entries : unchecked_downloads.entrySet())
            entries.getValue().delete();

        for (Entry<String, TempContentData[]> entries : zipContents.entrySet())
            for (TempContentData file : entries.getValue())
                file.file.delete();

        for (File file : this.toDelete)
            file.delete();
    }

    /**
     * Current Plugin Format:
     * {@code [<Flags>@]<Name>[:<Version>]}<p />
     * Current flags:
     * i: Ignore additional plugins installed by this plugin.
     * u: Force Update the plugin.
     *
     * @param name
     * @return
     * @throws java.io.IOException
     */
    private void downloadPlugin(String name) throws IOException {
        String[] data = this.parsePluginName(name);

        PluginDescription description = this.getDescription(data[1], data[2]);
        sender.sendMessage(_(this.sender, "installer.download.prepare", description.getName()));

        if (description==null)
            throw new FileNotFoundException(_(this.sender, "installer.download.notfound"));

        String filename = description.getFileName();
        String link = description.getDownload();
        String realName = description.getName();

        sender.sendMessage(_(this.sender, "installer.download.download", filename));

        // Download File...
        File f = File.createTempFile("binst_" + description.getFileName(), ".tmp");
        URL url = new URL(link);

        new DownloadTask(f, url).run();

        if (filename.endsWith(".zip")) {
            sender.sendMessage(_(this.sender, "installer.download.unpack.description", filename));
            unpackPlugin(name, f, realName, data[0].contains("i"));
        } else {
            unchecked_downloads.put(realName.toLowerCase(), f);
            download_names.put(realName.toLowerCase(), filename);
        }
    }

    /**
     * Unpacks the plugin.
     *
     * @param name The name of the plugin.
     * @param f The zip-file.
     * @throws java.io.IOException
     */
    private void unpackPlugin(String name, File f, String real_name, boolean ignore) throws IOException {
        ZipFile zip = new ZipFile(f);
        Enumeration<? extends ZipEntry> entries = zip.entries();

        List<TempContentData> data = new ArrayList<TempContentData>();
        File jar = null;


        while (entries.hasMoreElements()) {
            ZipEntry e = entries.nextElement();
            String rawname = e.getName();

            // Ignore directories...
            if (rawname.endsWith("/"))
                continue;

            // Extract Path data...
            String pathName = rawname.lastIndexOf('/') >= 0 ? rawname.substring(0, rawname.lastIndexOf('/')) : "";
            String fileName = rawname.lastIndexOf('/') >= 0 ? rawname.substring(rawname.lastIndexOf('/') + 1) : rawname;

            // Creating Meta-Data...
            File relPath = new File(pathName, fileName);
            File tempPath = File.createTempFile("zip_" + name, ".tmp");

            // Copy data...
            FileOutputStream fos = new FileOutputStream(tempPath);
            fos.getChannel().transferFrom(Channels.newChannel(zip.getInputStream(e)), 0, 1 << 24);
            fos.close();

            // Check if the file is the plugin...
            if (fileName.endsWith(".jar") && pathName.isEmpty()) {
                PluginDescriptionFile description = this.list.getDescription(tempPath);
                if (description == null) {
                    data.add(new TempContentData(relPath, tempPath));
                    continue;
                }

                if (jar == null && description.getName().equalsIgnoreCase(real_name)) {
                    jar = tempPath;
                    download_names.put(real_name.toLowerCase(), fileName);
                    continue;
                } else if (ignoreAdditionalPlugins || ignore) {
                    sender.sendMessage(_(this.sender, "installer.download.unpack.ignore", fileName));
                    toDelete.add(tempPath);
                    continue;
                }

            }

            // Register file
            data.add(new TempContentData(relPath, tempPath));
        }

        zip.close();

        if (jar == null) {
            throw new FailedExecutionException(_(this.sender, "installer.download.unpack.invalid"));
        }

        unchecked_downloads.put(real_name.toLowerCase(), jar);
        zipContents.put(real_name.toLowerCase(), data.toArray(new TempContentData[0]));
    }

    /**
     * Downloads the description and caches it.
     * @param name
     * @param version
     * @return
     */
    private PluginDescription getDescription(String name, String version) {
        if (cache.containsKey(name + ":" + version))
            return cache.get(name + ":" + version);

        PluginDescription description = this.searcher.getDescription(name, version);
        if (description == null)
            return null;

        cache.put(name + ":" + version, description);
        return description;
    }

    /**
     * Parses the plugin name.
     * @param raw
     * @return
     */
    private String[] parsePluginName(String raw) {
        String flags = raw.contains("@") ? raw.split("@", 2)[0] : "";
        String pldata = raw.contains("@") ? raw.split("@", 2)[1] : raw;
        String version = pldata.contains(":") ? pldata.split(":", 2)[1] : "latest";
        String name = pldata.contains(":") ? pldata.split(":", 2)[0] : pldata;

        return new String[] {flags, name, version};
    }

    /**
     * Set this to true if SoftDependencies should also be installed.
     *
     * @param useSoftDepend
     */
    public void setUseSoftDepend(boolean useSoftDepend) {
        this.useSoftDepend = useSoftDepend;
    }

    /**
     * Set this to true if the plugins should be installed.
     *
     * @param forceUpdate
     */
    public void setForceUpdate(boolean forceUpdate) {
        this.forceUpdate = forceUpdate;
    }

    /**
     * Should the installer ignore additional plugins.
     *
     * @param ignoreAdditionalPlugins
     */
    public void setIgnoreAdditionalPlugins(boolean ignoreAdditionalPlugins) {
        this.ignoreAdditionalPlugins = ignoreAdditionalPlugins;
    }
}
