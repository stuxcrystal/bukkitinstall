/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.executors;

import net.stuxcrystal.bukkitinstall.MessageReceiver;
import net.stuxcrystal.bukkitinstall.api.Migrator;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * The internal class handling the migrates.<p />
 *
 * This is a singleton.
 */
public final class MigrateExecutor {

    private Map<String, Migrator> migrators = new HashMap<String, Migrator>();

    private static final MigrateExecutor INSTANCE = new MigrateExecutor();

    private MigrateExecutor() {}

    /**
     * Registers a migrators to the system.
     * @param migrator The migrator that migrates a plugin file.
     */
    public void registerMigrator(Migrator migrator) {
        if (migrator.getName() == null)
            return;

        migrators.put(migrator.getName().toLowerCase(), migrator);
    }

    /**
     * Removes a migrator.
     * @param name The migra
     */
    public void removeMigrator(String name) {
        migrators.remove(name);
    }

    /**
     * Migrates the plugins of a updater-plugin.
     *
     * @param sender The sender of the command.
     * @param name The name of the plugin.
     * @param arguments The arguments submitted to the migrator.
     * @throws IOException if an I/O-Operation fails.
     */
    public void migrate(MessageReceiver sender, String name, String[] arguments) throws IOException {
        Migrator migrator = migrators.get(name.toLowerCase());

        if (migrator == null)
            throw new FileNotFoundException("migrators is not known.");

        migrator.execute(sender, arguments);
    }

    /**
     * Retrieves the instance of the Singleton.
     * @return A MigrateExecutor-Instance.
     */
    public static MigrateExecutor getInstance() {
        return INSTANCE;
    }

}
