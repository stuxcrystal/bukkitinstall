/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.executors;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.stuxcrystal.bukkitinstall.BukkitInstallPlugin;
import net.stuxcrystal.bukkitinstall.api.databases.PluginDescription;
import org.bukkit.plugin.PluginDescriptionFile;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import net.stuxcrystal.bukkitinstall.api.PluginList;
import net.stuxcrystal.bukkitinstall.tasks.RequestSiteTask;
import net.stuxcrystal.bukkitinstall.utils.IOUtil;

/**
 * Checks the plugins for updates.
 */
public class Checker {

    /**
     * The plugin-list the update-checker is operating with.
     */
    private PluginList list;

    /**
     * Constructor.
     * @param list The PluginList the server is operating with.
     */
    public Checker(PluginList list) {
        this.list = list;
    }

    /**
     * Returns updates using the given plugins.
     *
     * @param plugins the plugins to check.
     * @return All plugins that have to be checked.
     */
    public List<String> getUpdates(String[] plugins) {
        List<String> result = new ArrayList<String>();

        for (String plugin : plugins) {
            if (hasUpdate(plugin))
                result.add(plugin);
        }

        return result;
    }

    /**
     * Get all plugins to be updated.
     *
     * @return returns all updates.
     */
    public List<String> getUpdates() {
        return getUpdates(this.list.getPluginNames());
    }

    /**
     * Checks if a plugin has an update.
     * @param name The name of the plugin
     * @return true, if the plugin has an update.
     * @throws IOException if the execution fails.
     */
    private boolean hasUpdate(String name) {
        PluginDescriptionFile pdf = this.list.getDescription(name);
        String version = pdf.getVersion();
        String pluginname = pdf.getName().toLowerCase();

        PluginDescription pd = BukkitInstallPlugin.getInstance().getPluginSearcher().getDescription(pluginname);
        if (pd == null) {
            return false; // Assume there is no update.
        }

        return !version.equalsIgnoreCase(pd.getVersion());

    }

}
