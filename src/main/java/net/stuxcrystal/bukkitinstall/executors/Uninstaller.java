/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.executors;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.PluginClassLoader;

import net.stuxcrystal.bukkitinstall.BukkitInstallPlugin;
import net.stuxcrystal.bukkitinstall.FailedExecutionException;
import net.stuxcrystal.bukkitinstall.MessageReceiver;
import net.stuxcrystal.bukkitinstall.api.PluginList;
import net.stuxcrystal.bukkitinstall.utils.MiscUtils;

import static net.stuxcrystal.bukkitinstall.l10n.Translations._;

/**
 * Uninstalles a plugin and all plugins that depends on the plugin.
 *
 * @author StuxCrystal
 */
public class Uninstaller {

    /**
     * The plugin directory list.
     */
    private PluginList list;

    /**
     * List of plugins to be deleted...
     */
    private List<File> checkedPlugins = new ArrayList<File>();

    /**
     * List of plugins to be checked
     */
    private List<File> uncheckedPlugins = new ArrayList<File>();

    /**
     * Plugins to be shutdown before the action can take place.
     */
    private List<File> checkedShutdown = new ArrayList<File>();

    /**
     * Plugins to be shutdown before the action can take place.
     */
    private List<File> uncheckedShutdown = new ArrayList<File>();

    /**
     * The class loader of the plugin.
     */
    private List<PluginClassLoader> classLoaders = new ArrayList<PluginClassLoader>();

    /**
     * Output.
     */
    private MessageReceiver receiver;

    public Uninstaller(MessageReceiver receiver, PluginList list) {
        this.list = list;
        this.receiver = receiver;
    }

    /**
     * Uninstalls all given plugins.
     *
     * @param names A list of the names of the plugin.
     * @throws FailedExecutionException thrown when the execution fails.
     */
    public void uninstallPlugins(String[] names) throws FailedExecutionException {
        for (String name : names) {
            File f = list.getPluginFile(name);
            if (f == null) {
                throw new FailedExecutionException(_(receiver, "uninstall.unknown", f.getName()));
            }

            uncheckedPlugins.add(f);
        }

        receiver.sendMessage(_(receiver, "uninstall.dependencies"));
        // Calculate Hard-Dependency
        while (calculateHardDependencies()) ;
        uncheckedShutdown.addAll(checkedPlugins);

        // Calculate Soft-Dependency
        while (calculateSoftDependencies()) ;

        receiver.sendMessage(_(receiver, "uninstall.classloaders.aquire.title"));
        for (File file : this.checkedPlugins) {
            PluginClassLoader loader = this.list.getClassLoader(file);
            if (loader == null) {
                receiver.sendMessage(_(receiver, "uninstall.classloaders.aquire.fail", file.getName()));
                continue;
            }
            this.classLoaders.add(loader);
        }

        // Unload Plugins synchronously...
        Future<Void> wait = Bukkit.getScheduler().callSyncMethod(BukkitInstallPlugin.getInstance(), new Callable<Void>() {

            @Override
            public Void call() throws Exception {
                Uninstaller.this.disablePlugins();
                return null;
            }

        });

        while (!wait.isDone()) MiscUtils.sleep(200);

        receiver.sendMessage(_(receiver, "uninstall.classloaders.close.title"));
        for (PluginClassLoader loader : this.classLoaders) {
            try {
                loader.close();
            } catch (IOException e) {
                receiver.sendMessage(_(receiver, "uninstall.classloaders.close.fail"));
            }
        }

        receiver.sendMessage(_(receiver, "uninstall.sleep.title"));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            receiver.sendMessage(_(receiver, "uninstall.sleep.interrupt"));
            return;
        }

        receiver.sendMessage(_(receiver, "uninstall.delete.title"));
        for (File file : this.checkedPlugins)
            if (!file.delete())
                receiver.sendMessage(_(receiver, "uninstall.delete.fail", file.getName()));

        receiver.sendMessage(_(receiver, "uninstall.load"));
        for (File file : this.checkedShutdown)
            if (!this.checkedPlugins.contains(file))
                this.list.enablePlugin(file);

        receiver.sendMessage(_(receiver, "uninstall.done"));

    }

    /**
     * This two steps have to be executed synchronously because they use Bukkit API commands that are using shared lists.
     */
    private void disablePlugins() {
        receiver.sendMessage(_(receiver, "uninstall.disable.softdependencies"));
        for (File file : this.checkedShutdown)
            if (!this.checkedPlugins.contains(file))
                this.list.unloadPlugin(file);

        receiver.sendMessage(_(receiver, "uninstall.disable.harddependencies"));
        for (File file : this.checkedShutdown)
            this.list.unloadPlugin(file);
    }

    /**
     * Calculates the hard-dependencies...
     *
     * @return true if a dependency was added.
     */
    private boolean calculateHardDependencies() {
        List<File> plugins = new ArrayList<File>(uncheckedPlugins);
        checkedPlugins.addAll(plugins);
        uncheckedPlugins.clear();

        boolean addedDependency = false;

        for (File f : plugins) {
            List<File> files = list.getHardDependentPlugins(f);
            if (files != null && !files.isEmpty()) {
                for (File dep : files) {
                    if (!uncheckedPlugins.contains(dep) && !checkedPlugins.contains(dep)) {
                        uncheckedPlugins.add(dep);
                    }
                }
                addedDependency = true;
            }
        }

        return addedDependency;
    }

    /**
     * Calculates the soft-dependencies...
     *
     * @return
     */
    private boolean calculateSoftDependencies() {
        List<File> plugins = new ArrayList<File>(uncheckedShutdown);
        checkedShutdown.addAll(plugins);
        uncheckedShutdown.clear();

        boolean addedDependency = false;

        for (File f : plugins) {
            List<File> files = list.getSoftDependentPlugins(f);
            if (files != null && !files.isEmpty()) {
                for (File dep : files) {
                    if (!uncheckedShutdown.contains(dep) && !checkedShutdown.contains(dep)) {
                        uncheckedShutdown.add(dep);
                    }
                }
                addedDependency = true;
            }
        }

        return addedDependency;
    }

}
