/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.executors;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import net.stuxcrystal.bukkitinstall.BukkitInstallPlugin;
import net.stuxcrystal.bukkitinstall.api.PluginList;

import org.bukkit.plugin.Plugin;

/**
 * Manages dependencies.<p />
 * This is a internal class. It is usually better to use PluginList to manage dependencies.
 *
 * @author StuxCrystal
 */
public class DependencyManager {

    /**
     * The plugin list to manage its dependencies.
     */
    private PluginList list;

    /**
     * The dependencies which dependencies are not checked.
     */
    private List<File> unchecked_dependencies = new ArrayList<File>();

    /**
     * The dependencies which dependencies are not checked.
     */
    private List<File> checked_dependencies = new ArrayList<File>();

    /**
     * Internal constructor.
     *
     * @param list
     */
    private DependencyManager(PluginList list) {
        this.list = list;
    }

    /**
     * Checks if the dependency is already known.
     *
     * @param file
     * @return
     */
    private boolean isKnown(File file) {
        return unchecked_dependencies.contains(file) || checked_dependencies.contains(file);
    }

    /**
     * Calculates the dependencies.
     */
    public void calculateDependencies() {
        while (calculate()) ;
    }

    /**
     * Calculates the dependencies.
     *
     * @return
     */
    private boolean calculate() {
        List<File> dependencies = new ArrayList<File>(unchecked_dependencies);
        unchecked_dependencies.clear();
        checked_dependencies.addAll(dependencies);

        for (File f : dependencies) {
            for (File dep : this.list.getHardDependentPlugins(f))
                if (!isKnown(dep))
                    unchecked_dependencies.add(dep);
        }

        return !unchecked_dependencies.isEmpty();
    }

    public void addDependency(File f) {
        if (!isKnown(f))
            this.unchecked_dependencies.add(f);
    }

    /**
     * Returns a list of dependencies.
     *
     * @return
     */
    public List<File> getCheckedDependencies() {
        return this.checked_dependencies;
    }

    /**
     * Calculates the dependencies and adds all.
     *
     * @param file The dependency to check.
     * @return the dependency.
     */
    public static List<File> getDependencies(File file) {
        DependencyManager manager = new DependencyManager(BukkitInstallPlugin.getInstance().getPluginList());
        manager.addDependency(file);
        manager.calculateDependencies();
        List<File> result = manager.getCheckedDependencies();
        result.remove(file);
        return result;
    }

    /**
     * Calculates the dependencies of the file.
     *
     * @param plugin
     * @return
     */
    public static List<File> getDependencies(Plugin plugin) {
        return getDependencies(BukkitInstallPlugin.getInstance().getPluginList().getPluginFile(plugin));
    }

}
