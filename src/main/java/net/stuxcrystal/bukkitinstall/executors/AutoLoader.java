/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.executors;

import net.stuxcrystal.bukkitinstall.BukkitInstallPlugin;
import net.stuxcrystal.bukkitinstall.ConfigurationFile;
import org.apache.commons.lang.ArrayUtils;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginLoadOrder;
import org.bukkit.plugin.PluginManager;
import org.bukkit.scheduler.BukkitScheduler;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Auto-Loads plugins.
 *
 * @author stuxcrystal
 */
public class AutoLoader {

    /**
     * The configuration file.
     */
    private ConfigurationFile config;

    /**
     * A reference to the BukkitInstall plugin.
     */
    private BukkitInstallPlugin plugin;

    /**
     * A reference to the PluginManager.
     */
    private PluginManager manager;

    /**
     * The BukkitScheduler
     */
    private BukkitScheduler scheduler;

    /**
     * Plugins to load.
     */
    private Plugin[] plugins = new Plugin[0];

    public class PostWorldLoader implements Runnable {
        private Plugin[] plugins;

        public PostWorldLoader(Plugin[] plugins) {
            this.plugins = plugins;
        }

        public void run() {
            for (Plugin plugin : plugins) {
                try {
                    AutoLoader.this.manager.enablePlugin(plugin);
                } catch (Throwable e) {
                    AutoLoader.this.plugin.getLogger().log(
                        Level.WARNING,
                        "Failed to load plugin: " + plugin.getDescription().getFullName(),
                        e
                    );
                }
            }
        }
    }

    /**
     * Constructs the AutoLoader.
     * @param config
     * @param plugin
     */
    public AutoLoader(ConfigurationFile config, BukkitInstallPlugin plugin, PluginManager manager,
                      BukkitScheduler scheduler) {
        this.config = config;
        this.plugin = plugin;
        this.manager = manager;
        this.scheduler = scheduler;
    }

    /**
     * Loads the plugins.
     */
    public void loadPlugins() {

        // Auto-Load directories.
        for (String path : this.config.getAutoLoadDirectories()) {
            File directory = new File(path);
            this.plugins = (Plugin[]) ArrayUtils.addAll(this.plugins, this.manager.loadPlugins(directory));
        }

        // Sort the plugins using the start-time.
        List<Plugin> startup = new ArrayList<Plugin>();
        List<Plugin> postWorld = new ArrayList<Plugin>();

        for (Plugin plugin : plugins) {
            PluginDescriptionFile pdf = plugin.getDescription();
            switch (pdf.getLoad()) {
                case STARTUP:
                    startup.add(plugin);
                    break;

                case POSTWORLD:
                    postWorld.add(plugin);
                    break;
            }
        }

        // Enable Plugins that are enabled at startup.
        for (Plugin plugin : startup) {
            this.manager.enablePlugin(plugin);
        }

        // Load Plugins that are enabled after the worlds are loaded.
        this.scheduler.scheduleSyncDelayedTask(this.plugin, new PostWorldLoader(postWorld.toArray(new Plugin[0])), 1);
    }
}
