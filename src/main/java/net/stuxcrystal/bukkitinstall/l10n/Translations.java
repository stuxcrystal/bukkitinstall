/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.l10n;

import net.stuxcrystal.bukkitinstall.BukkitInstallPlugin;
import net.stuxcrystal.bukkitinstall.ConfigurationFile;
import net.stuxcrystal.bukkitinstall.MessageReceiver;
import net.stuxcrystal.bukkitinstall.tasks.StreamCopyTask;
import net.stuxcrystal.bukkitinstall.utils.CommandSenderReceiver;
import net.stuxcrystal.bukkitinstall.utils.MapStringFormatter;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Description
 *
 * @author stuxcrystal
 */
public class Translations {

    /**
     * Current configuration file.
     */
    private ConfigurationFile config;

    /**
     * A Reference to the configuration time.
     */
    private BukkitInstallPlugin plugin;

    /**
     * The configuration cache.
     */
    private Map<String, Configuration> cache = new HashMap<String, Configuration>();

    /**
     * The instance of the translations.
     */
    private static Translations TRANSLATIONS = null;

    public Translations(ConfigurationFile config, BukkitInstallPlugin plugin) {
        this.config = config;
        this.plugin = plugin;

        TRANSLATIONS = this;

        this.reset();

    }

    public void copyLangFile(String lang, boolean reset) throws IOException {
        File file = this.config.getTranslationDirectory();
        if (!file.exists()) {
            file.mkdirs();
        }



        File f = new File(this.config.getTranslationDirectory(), lang + ".yml");
        InputStream is = null;
        if ((is = this.plugin.getResource("additional/lang/" + lang + ".yml")) == null) {
            is = this.plugin.getResource("tl_default.yml");

        }

        if (reset || !f.exists()) {
            plugin.getLogger().info("[Localization] Copy " + lang + ".yml to translation directory.");
            new StreamCopyTask(is, f).run();
        }
    }

    /**
     * Prepares the default language file.
     */
    public void prepare(boolean reset) throws IOException {
        copyLangFile(Locale.getDefault().getLanguage(), reset);
    }

    /**
     * Returns the default translation.
     * @return
     */
    private Configuration getDefault() {

        if (this.cache.containsKey("default")) {
            return this.cache.get("default");
        }

        YamlConfiguration config = YamlConfiguration.loadConfiguration(
                plugin.getResource("tl_default.yml")
        );

        this.cache.put("default", config);
        return config;
    }

    /**
     * Returns the configuration from the cache, or if the plugin-configuration was reloaded,
     * directly from the file.
     *
     * @param name The key of the cache.
     * @param file The file to fetch.
     * @return
     */
    private Configuration getConfiguration(String name, File file, Configuration def) {
        if (this.cache.containsKey(name))
            return this.cache.get(name);

        YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
        config.setDefaults(def);
        this.cache.put(name, config);
        return config;

    }

    /**
     * Returns the localization for the language.
     * @param lang
     * @return
     */
    private Configuration getLocalization(String lang) {
        File langFile = new File(this.config.getTranslationDirectory(), (lang + ".yml").toLowerCase());
        if (!langFile.exists()) {
            try {
                copyLangFile(lang.toLowerCase(), false);
            } catch (IOException e) {
                e.printStackTrace();
                return getDefault();
            }
        }

        return getConfiguration("tl:" + lang.toLowerCase(), langFile, this.getDefault());
    }

    /**
     * Constructs a map with colors as variables.
     * @param args
     * @return
     */
    private Map<String, String> constructVariables(String... args) {
        Map<String, String> values = new HashMap<String, String>();
        for (ChatColor color : ChatColor.values()) {
            values.put("color:" + color.name(), new String(new char[]{ChatColor.COLOR_CHAR, color.getChar()}));
        }

        for (int i = 0; i<args.length; i++) {
            values.put("args:" + i, args[i]);
        }

        return values;
    }

    /**
     * Multi line translation.<p />
     *
     * Can now perfo
     * @param locale
     * @param key
     * @param args
     * @return
     */
    public String[] translate(Locale locale, String key, String... args) {
        Configuration config = getLocalization(locale.getLanguage());
        List<String> raw = null;

        if (config.isString(key))
            raw = Arrays.asList(config.getString(key));
        else if (config.isList(key))
            raw = config.getStringList(key);

        if (raw == null)
            raw = Arrays.asList(key);

        Map<String, String> variables = constructVariables(args);

        for (int i = 0; i<raw.size(); i++)
            raw.set(i, MapStringFormatter.format(raw.get(i), constructVariables(args)));

        return raw.toArray(new String[0]);
    }

    /**
     * Resetsthe configuration.
     */
    public void reset() {
        this.cache.clear();

        try {
            this.prepare(false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Translates a string. (Multi-Line available)
     * @param sender
     * @param message
     * @param args
     * @return
     */
    public static final String[] _M(MessageReceiver sender, String message, String... args) {
        return TRANSLATIONS.translate(sender.getLocale(), message, args);
    }

    /**
     * Returns a translated string.
     * @param sender
     * @param message
     * @param args
     * @return
     */
    public static final String _(MessageReceiver sender, String message, String... args) {
        return TRANSLATIONS.translate(sender.getLocale(), message, args)[0];
    }

    /**
     * Translates a string.
     * @param sender
     * @param message
     * @param args
     * @return
     */
    public static final String[] _M(CommandSender sender, String message, String... args) {
        return _M(new CommandSenderReceiver(sender), message, args);
    }

    /**
     * Translates a string.
     * @param sender
     * @param message
     * @param args
     * @return
     */
    public static final String _(CommandSender sender, String message, String... args) {
        return _(new CommandSenderReceiver(sender), message, args);
    }

    /**
     * Returns the translation instance.
     * @return
     */
    public static Translations getInstance() {
        return TRANSLATIONS;
    }
}
