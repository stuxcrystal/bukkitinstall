/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.l10n;

import net.stuxcrystal.bukkitinstall.MessageReceiver;
import net.stuxcrystal.bukkitinstall.utils.CommandSenderReceiver;
import org.bukkit.command.CommandSender;

import static net.stuxcrystal.bukkitinstall.l10n.Translations._;

/**
 * Helper for headers.
 *
 * @author stuxcrystal
 */
public class Headers {

    public static String getHeader(MessageReceiver receiver, String title) {
        return _(receiver, "headers.pre") + title + _(receiver, "headers.post");
    }

    public static String getHeader(CommandSender sender, String title) {
        return getHeader(new CommandSenderReceiver(sender), title);
    }

    public static String getHeaderPaginized(CommandSender sender, String title, int page, int count) {
        return getHeaderPaginized(new CommandSenderReceiver(sender), title, page, count);
    }

    public static String getHeaderPaginized(MessageReceiver receiver, String title, int page, int count) {
        return _(receiver, "headers.pre") + title + _(receiver, "headers.pages", ""+page, ""+count) +
                _(receiver, "headers.post");
    }

}
