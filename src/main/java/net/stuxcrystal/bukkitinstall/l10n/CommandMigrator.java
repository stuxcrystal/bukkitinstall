/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.l10n;

import net.stuxcrystal.bukkitinstall.commandhandler.TranslationHandler;
import net.stuxcrystal.bukkitinstall.utils.CommandSenderReceiver;
import org.bukkit.command.CommandSender;

/**
 * Translates the message to another language.
 *
 * @author stuxcrystal
 */
public class CommandMigrator implements TranslationHandler {
    @Override
    public String getTranslation(CommandSender sender, String key) {
        return Translations._(new CommandSenderReceiver(sender), key);
    }
}
