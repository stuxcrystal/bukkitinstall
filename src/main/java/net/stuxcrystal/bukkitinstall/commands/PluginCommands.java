/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.commands;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import net.stuxcrystal.bukkitinstall.api.databases.PluginDescription;
import net.stuxcrystal.bukkitinstall.l10n.Headers;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.PluginDescriptionFile;

import net.stuxcrystal.bukkitinstall.BukkitInstallPlugin;
import net.stuxcrystal.bukkitinstall.api.PluginList;
import net.stuxcrystal.bukkitinstall.api.PluginStatus;
import net.stuxcrystal.bukkitinstall.commandhandler.ArgumentParser;
import net.stuxcrystal.bukkitinstall.commandhandler.Command;

import static net.stuxcrystal.bukkitinstall.l10n.Translations._;
import static net.stuxcrystal.bukkitinstall.l10n.Translations._M;

/**
 * Lists all plugins or gives information about them.
 *
 * @author StuxCrystal
 */
public class PluginCommands {

    /**
     * Maximal amount of plugins per page.
     */
    private static final int MAX_PAGE_SIZE = 5;

    /**
     * A reference to bukkit install.
     */
    BukkitInstallPlugin install;

    public PluginCommands(BukkitInstallPlugin install) {
        this.install = install;
    }

    /**
     * Lists commands.
     *
     * @param sender
     * @param arguments
     */
    @Command(value = "list", permission = "bukkitinstall.list", maxSize = 1, flags = "ns", description = "help.entries.list", aliases = {"plugins", "pl"})
    public void listPlugins(CommandSender sender, ArgumentParser arguments) {
        PluginList list = install.getPluginList();

        // Show the short plugin list. (Bukkit variant.)
        // Please not that this is NOT intended to be translatable.
        if (arguments.hasFlag('s')) {
            // Also show the unloaded plugins now.
            StringBuilder builder = new StringBuilder();

            Map<File, PluginDescriptionFile> plugins = list.getExistingPlugins();
            builder.append("Plugins (").append(plugins.size()).append("): ");

            boolean first = true;
            for (Entry<File, PluginDescriptionFile> plugin : plugins.entrySet()) {
                if (!first) {
                    builder.append(ChatColor.RESET + ", ");
                } else {
                    first = false;
                }

                ChatColor c;
                switch (list.getStatus(plugin.getKey())) {
                    case UNLOADED:
                        c = ChatColor.RED;
                        break;

                    case DEACTIVATED:
                        c = ChatColor.YELLOW;
                        break;

                    case ACTIVATED:
                        c = ChatColor.GREEN;
                        break;

                    default:
                        c = ChatColor.GRAY;
                        // Should not be happening...
                }

                builder.append(c);
                builder.append(plugin.getValue().getName());
            }

            // Sends a message to the sender.
            sender.sendMessage(builder.toString());
            return;
        }

        // This is intended to be localizable.

        Map<File, PluginDescriptionFile> plugins = new TreeMap<File, PluginDescriptionFile>(list.getExistingPlugins());
        int size = arguments.hasFlag('n') ? plugins.size() : MAX_PAGE_SIZE;
        int pages = (int) Math.ceil(plugins.size() / (double) size);
        int position = Math.min((arguments.count() > 0 ? NumberUtils.toInt(arguments.getString(0), 1) : 1), pages) - 1;

        if (!arguments.hasFlag('n')) {
            sender.sendMessage(Headers.getHeaderPaginized(sender, _(sender, "list.title"), (position + 1), pages));
        } else {
            sender.sendMessage(Headers.getHeader(sender, _(sender, "list.title")));
        }
        int i = 0;
        for (Entry<File, PluginDescriptionFile> plugin : plugins.entrySet()) {
            if ((i++) < size * position) {
                continue;
            } else if (i - 1 >= size * (position + 1))
                break;

            switch (list.getStatus(plugin.getKey())) {
                case UNLOADED:
                    sender.sendMessage(_(sender, "list.entry.unloaded", plugin.getValue().getName(), plugin.getKey().getName()));
                    break;

                case DEACTIVATED:
                    sender.sendMessage(_(sender, "list.entry.disabled", plugin.getValue().getName(), plugin.getKey().getName()));
                    break;

                case ACTIVATED:
                    sender.sendMessage(_(sender, "list.entry.enabled", plugin.getValue().getName(), plugin.getKey().getName()));
                    break;
            }
        }
    }

    /**
     * Prints info about the plugin.
     *
     * @param sender
     * @param arguments
     */
    @Command(value = "info", permission = "bukkitinstall.info", maxSize = 1, minSize = 1, flags = "d", description = "help.entries.info", async = true, aliases = {"version", "ver"})
    public void infoPlugin(CommandSender sender, ArgumentParser arguments) {
        PluginList list = install.getPluginList();
        File file = list.getPluginFile(arguments.getString(0));

        Map<String, String> data = new LinkedHashMap<String, String>();

        if (arguments.hasFlag('d') || file == null) {
            // Download the description data from bukget.
            PluginDescription description = install.getPluginSearcher().getDescription(arguments.getString(0));

            if (description == null) {
                sender.sendMessage(_(sender, "info.notfound"));
                return;
            }

            data.put("name", description.getName());
            data.put("description", description.getDescription());
            data.put("version", description.getVersion());
            data.put("author", StringUtils.join(description.getAuthors(), ", "));
            data.put("file", description.getFileName());
            data.put("status", file == null ? _(sender, "info.keys.state.uninstalled") : _(sender, "info.keys.state.installed"));

        } else {
            // Fetch the data from the description file.
            PluginDescriptionFile pdf = list.getDescription(file);
            PluginStatus status = list.getStatus(file);

            data.put("name", pdf.getName());
            data.put("description", pdf.getDescription());
            data.put("version", pdf.getVersion());
            data.put("author", StringUtils.join(pdf.getAuthors(), ", "));
            data.put("file", file.getName());
            data.put("status", _(sender, "info.keys.state." + status.name().toLowerCase()));

            List<String> names = new ArrayList<String>();
            for (File f : list.getHardDependentPlugins(file))
                names.add(list.getName(f));
            for (File f : list.getSoftDependentPlugins(file))
                names.add(list.getName(f));

            data.put("used", StringUtils.join(names, ", "));

        }

        sender.sendMessage(Headers.getHeader(sender, _(sender, "info.title")));
        for (Entry<String, String> entry : data.entrySet()) {
            String key = _(sender, "info.keys.name." + entry.getKey());
            sender.sendMessage(_(sender, "info.entry", key, entry.getValue()));
        }
    }

    /**
     * Reloads a plugin.<p />
     * <p/>
     * The -s Flag forces the plugin to execute a soft reload.
     * The -h Flag forces the plugin to close its ClassLoader after it is disabled.
     *
     * @param sender
     * @param arguments
     */
    @Command(value = "reload", permission = "bukkitinstall.reload", flags = "sh", maxSize = 1, minSize = 1, description = "help.entries.reload", async = false)
    public void reloadPlugin(CommandSender sender, ArgumentParser arguments) {
        if (arguments.hasFlag('s') && arguments.hasFlag('h')) {
            sender.sendMessage(_(sender, "control.sh-error"));
            return;
        }

        PluginList list = install.getPluginList();
        File file = list.getPluginFile(arguments.getString(0));
        if (arguments.hasFlag('s')) {
            list.disablePlugin(file);
            list.enablePlugin(file);
        } else {
            if (arguments.hasFlag('h')) {
                list.hardUnloadPlugin(file);
            }
            list.enablePlugin(file);
        }
        sender.sendMessage(_(sender, "control.reload"));
    }

    /**
     * Disables a plugin.<p />
     * <p/>
     * The -s Flag forces the plugin to execute a soft disable.
     * The -h Flag forces the plugin to close its ClassLoader.
     *
     * @param sender
     * @param arguments
     */
    @Command(value = "disable", permission = "bukkitinstall.disable", flags = "sh", maxSize = 1, minSize = 1, description = "help.entries.disable", async = false, aliases = {"unload"})
    public void disablePlugin(CommandSender sender, ArgumentParser arguments) {
        if (arguments.hasFlag('s') && arguments.hasFlag('h')) {
            sender.sendMessage(_(sender, "control.sh-error"));
            return;
        }

        PluginList list = install.getPluginList();
        File file = list.getPluginFile(arguments.getString(0));
        if (arguments.hasFlag('s')) {
            list.disablePlugin(file);
        } else {
            if (arguments.hasFlag('h')) {
                list.hardUnloadPlugin(file);
            } else {
                list.unloadPlugin(file);
            }
        }
        sender.sendMessage(_(sender, "control.disable"));
    }

    /**
     * Enables the plugin.<p />
     * Loads it before if it wasn't loaded before.
     *
     * @param sender
     * @param arguments
     */
    @Command(value = "enable", permission = "bukkitinstall.enable", maxSize = 1, minSize = 1, description = "help.entries.enable", async = false, aliases = {"load"})
    public void enablePlugin(CommandSender sender, ArgumentParser arguments) {
        PluginList list = install.getPluginList();
        File file = list.getPluginFile(arguments.getString(0));
        list.enablePlugin(file);
        sender.sendMessage(_(sender, "control.enable"));
    }

    /**
     * Searches for plugins. Each page stands for a plugin database.<p />
     *
     * Params:<br />
     * p: Page | The first attribute is treated as the page.
     * d: Detailed View
     * @param sender
     * @param arguments
     */
    @Command(value = "search", permission = "bukkitinstall.search", minSize = 1, description = "help.entries.search", async = true, flags="dp")
    public void searchPlugins(CommandSender sender, ArgumentParser arguments) {
        int page = 0;
        String[] args = arguments.getArguments(0);

        if (arguments.hasFlag('p')) {
            page = arguments.getInt(0, 1) - 1;
            args = arguments.getArguments(1);
        }

        List<PluginDescription> plugins = install.getPluginSearcher().searchPlugins(StringUtils.join(args, " "), page, 10);
        for (PluginDescription plugin : plugins) {
            if (arguments.hasFlag('d')) {
                sender.sendMessage(_M(sender, "search.detailed.message", plugin.getName(), plugin.getDescription(), plugin.getVersion()));
            } else {
                sender.sendMessage(_(sender, "search.simple.text", plugin.getName(), plugin.getVersion()));
            }
        }
        if (arguments.hasFlag('d')) {
            sender.sendMessage(_(sender, "search.detailed.endoflist"));
        }
    }
}
