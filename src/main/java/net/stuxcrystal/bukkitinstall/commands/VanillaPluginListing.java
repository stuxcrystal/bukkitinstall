/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.commands;

import java.io.File;
import java.util.Map;
import java.util.Map.Entry;

import net.stuxcrystal.bukkitinstall.BukkitInstallPlugin;
import net.stuxcrystal.bukkitinstall.api.PluginList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.ServerCommandEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;

public class VanillaPluginListing implements Listener {

    @EventHandler(ignoreCancelled = true)
    public void checkPlugin(PlayerCommandPreprocessEvent event) {
        if (event.getMessage().startsWith("/plugins ") || event.getMessage().startsWith("/pl ")) {
            // Ignore this if the user does not have rights to use /bi list
            if (!event.getPlayer().hasPermission("bukkitinstall.list"))
                return;

            onCommand(event.getPlayer());
            event.setCancelled(true);
        } else if (event.getMessage().startsWith("/ver ") || event.getMessage().startsWith("/version ")) {
            // Ignore this if the user does not have rights to use /bi info.
            if (!event.getPlayer().hasPermission("bukkitinstall.info"))
                return;

            event.setMessage(event.getMessage().replaceFirst("ver(sion)? ", "bukkitinstall info "));
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void hookConsole(ServerCommandEvent event) {
        if (event.getCommand().equalsIgnoreCase("pl") || event.getCommand().equalsIgnoreCase("plugins")) {
            event.setCommand("bukkitinstall list -s");
        } else if (event.getCommand().startsWith("ver ") || event.getCommand().startsWith("version ")) {
            event.setCommand(event.getCommand().replaceFirst("ver(sion)? ", "bukkitinstall info "));
        }
    }

    public boolean onCommand(CommandSender sender) {
        sender.sendMessage("BukkitInstall");

        if (sender.hasPermission("bukkitinstall.list")) {
            PluginList pllist = BukkitInstallPlugin.getInstance().getPluginList();

            // Also show the unloaded plugins now.
            StringBuilder builder = new StringBuilder();

            Map<File, PluginDescriptionFile> plugins = pllist.getExistingPlugins();
            builder.append("Plugins (").append(plugins.size()).append("): ");

            boolean first = true;
            for (Entry<File, PluginDescriptionFile> plugin : plugins.entrySet()) {
                if (!first) {
                    builder.append(ChatColor.RESET + ", ");
                } else {
                    first = false;
                }

                ChatColor c;
                switch (pllist.getStatus(plugin.getKey())) {
                    case UNLOADED:
                        c = ChatColor.RED;
                        break;

                    case DEACTIVATED:
                        c = ChatColor.YELLOW;
                        break;

                    case ACTIVATED:
                        c = ChatColor.GREEN;
                        break;

                    default:
                        c = ChatColor.GRAY;
                        // Should not be happening...
                }

                builder.append(c);
                builder.append(plugin.getValue().getName());
            }

            // Sends a message to the sender.
            sender.sendMessage(builder.toString());
        } else if (sender.hasPermission("bukkit.command.plugins")) {
            // Only show the loaded plugins.
            StringBuilder builder = new StringBuilder();

            Plugin[] plugins = Bukkit.getPluginManager().getPlugins();
            builder.append("Plugins (").append(plugins.length).append("): ");

            for (int i = 0; i < plugins.length; i++) {
                if (i > 0) {
                    builder.append(ChatColor.RESET + ", ");
                }

                builder.append(plugins[i].isEnabled() ? ChatColor.GREEN : ChatColor.RED);
                builder.append(plugins[i].getName());
            }

            // Sends a message to the sender.
            sender.sendMessage(builder.toString());
        } else {
            sender.sendMessage(ChatColor.RED + "Unfortunately you don't have permission to access the plugin list.");
        }

        return true;
    }

}
