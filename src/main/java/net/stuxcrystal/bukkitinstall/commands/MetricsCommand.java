/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.commands;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import net.stuxcrystal.bukkitinstall.l10n.Headers;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import net.stuxcrystal.bukkitinstall.BukkitInstallPlugin;
import net.stuxcrystal.bukkitinstall.commandhandler.ArgumentParser;
import net.stuxcrystal.bukkitinstall.commandhandler.Command;
import static net.stuxcrystal.bukkitinstall.l10n.Translations._;

public class MetricsCommand {

    private JavaPlugin plugin;

    public MetricsCommand() {
        this.plugin = BukkitInstallPlugin.getInstance();
    }

    /**
     * Returns the configuration-file of PluginMetrics.
     *
     * @return
     */
    private File getMetricsConfiguration() {
        File pluginsFolder = plugin.getDataFolder().getParentFile();
        return new File(new File(pluginsFolder, "PluginMetrics"), "config.yml");
    }

    private YamlConfiguration getConfiguration() {
        YamlConfiguration configuration = YamlConfiguration.loadConfiguration(getMetricsConfiguration());
        configuration.addDefault("opt-out", true);                                         // To prevent being tracked.
        configuration.addDefault("guid", UUID.randomUUID().toString());
        configuration.addDefault("debug", false);

        if (configuration.get("guid", null) == null) {
            configuration.options().header("BukkitInstall | http://mcstats.org").copyDefaults(true);
            try {
                configuration.save(getMetricsConfiguration());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return configuration;
    }

    private void save(YamlConfiguration configuration) {
        configuration.options().header("BukkitInstall | http://mcstats.org").copyHeader(true);
        try {
            configuration.save(getMetricsConfiguration());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Command("")
    public void base(CommandSender sender, ArgumentParser arguments) {
        YamlConfiguration config = getConfiguration();

        sender.sendMessage(Headers.getHeader(sender, _(sender, "metrics.title")));
        sender.sendMessage(_(sender, "metrics.status.text", _(sender, "metrics.status." + (config.getBoolean("opt-out") ? "stopped" : "running"))));
        sender.sendMessage(_(sender, "metrics.guid.text", config.getString("guid", _(sender, "metrics.guid.none"))));
        sender.sendMessage("");
        sender.sendMessage(_(sender, "metrics.help.list"));

    }

    @Command("help")
    public void help(CommandSender sender, ArgumentParser arguments) {
        sender.sendMessage(Headers.getHeader(sender, _(sender, "metrics.help.title")));
        for (String name : Arrays.asList("enable", "disable", "guid", "info")) {
            sender.sendMessage(_(sender, "metrics.help." + name));
        }
    }

    /**
     * Enables or disables PluginMetrics.
     */
    @Command(value = "optout", minSize = 0, maxSize = 1, permission = "bukkitinstall.metrics.optout")
    public void enable(CommandSender sender, ArgumentParser arguments) {
        YamlConfiguration config = getConfiguration();
        if (arguments.count() == 0) {
            sender.sendMessage(_(sender, "metrics.status.text", _(sender, "metrics.status." + (config.getBoolean("opt-out")?"stopped":"running"))));
            return;
        }

        boolean opted = arguments.getString(0).matches("[yY]([eE][sS])?|[tT]([rR][uU][eE])?|[oO][fF][fF]");
        boolean invalid = !opted && !arguments.getString(0).matches("[nN]([oO])?|[fF]([aA][lL][sS][eE])?|[oO][nN]");
        if (!invalid) {
            config.set("opt-out", opted);
            save(config);
        } else {
            sender.sendMessage(_(sender, "metrics.status.set.invalid"));
            return;
        }

        if (opted) {
            sender.sendMessage(_(sender, "metrics.status.set.disabled"));
        } else {
            sender.sendMessage(_(sender, "metrics.status.set.enabled"));
        }
    }

    /**
     * Enables or disables PluginMetrics.
     */
    @Command(value = "guid", minSize = 0, maxSize = 0, permission = "bukkitinstall.metrics.guid")
    public void guid(CommandSender sender, ArgumentParser arguments) {
        YamlConfiguration config = getConfiguration();
        sender.sendMessage(_(sender, "metrics.guid.text", config.getString("guid", _(sender, "metrics.guid.none"))));
        return;
    }

    /**
     * Prints the information that would be sent to <a href="http://mcstats.org/">MCStats</a><p />
     *
     * This does not include plugin-specific settings.
     *
     * @param sender
     * @param arguments
     */
    @Command(value = "info", minSize = 0, maxSize = 0, permission = "bukkitinstall.metrics.info")
    public void info(CommandSender sender, ArgumentParser arguments) {

        String onlineMode = Bukkit.getServer().getOnlineMode() ? "true" : "false";
        String serverVersion = Bukkit.getVersion();
        String playersOnline = "" + Bukkit.getServer().getOnlinePlayers().length;
        String osname = System.getProperty("os.name");
        String osarch = System.getProperty("os.arch");
        String osver = System.getProperty("os.version");
        String java_version = System.getProperty("os.version");
        String cores = "" + Runtime.getRuntime().availableProcessors();

        if (osarch.equals("amd64"))
            osarch = "x86_64";

        Map<String, String> data = new LinkedHashMap<String, String>();
        data.put("server", serverVersion);
        data.put("players", playersOnline);
        data.put("osname", osname);
        data.put("osarch", osarch);
        data.put("osversion", osver);
        data.put("cores", cores);
        data.put("online-mode", onlineMode);
        data.put("java_version", java_version);

        sender.sendMessage(Headers.getHeader(sender, _(sender, "metrics.title")));
        for (Entry<String, String> entry : data.entrySet()) {
            sender.sendMessage(ChatColor.GREEN + entry.getKey() + ChatColor.RESET + " : " + ChatColor.GREEN + entry.getValue());
        }
    }

}
