/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.commands;

import java.util.List;

import net.stuxcrystal.bukkitinstall.l10n.Headers;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.stuxcrystal.bukkitinstall.BukkitInstallPlugin;
import net.stuxcrystal.bukkitinstall.FailedExecutionException;
import net.stuxcrystal.bukkitinstall.commandhandler.ArgumentParser;
import net.stuxcrystal.bukkitinstall.commandhandler.Command;
import net.stuxcrystal.bukkitinstall.executors.Checker;
import net.stuxcrystal.bukkitinstall.executors.Installer;
import net.stuxcrystal.bukkitinstall.executors.Uninstaller;
import net.stuxcrystal.bukkitinstall.utils.CommandSenderReceiver;

import static net.stuxcrystal.bukkitinstall.l10n.Translations._;

/**
 * All commands that have something to do with installing plugins
 * are stored here.
 *
 * @author StuxCrystal
 */
public class InstallCommands {

    /**
     * A reference to bukkit install.
     */
    private BukkitInstallPlugin plugin;

    public InstallCommands(BukkitInstallPlugin plugin) {
        this.plugin = plugin;
    }

    /**
     * Installs one or more plugins.
     *
     * @param sender
     * @param args
     */
    @Command(value = "install", async = true, minSize = 1, flags = "sun", description = "help.entries.install", permission = "bukkitinstall.install")
    public void install(CommandSender sender, ArgumentParser args) {
        Installer installer = new Installer(new CommandSenderReceiver(sender), plugin.getPluginList(), plugin.getPluginSearcher());
        installer.setUseSoftDepend(args.hasFlag('s'));
        installer.setForceUpdate(args.hasFlag('u'));
        installer.setIgnoreAdditionalPlugins(args.hasFlag('n'));

        installer.installPlugins(args.getArguments(0));

        sender.sendMessage(_(sender, "installer.done"));
    }

    /**
     * Removes one or more plugins...<p />
     * Async, IO Operations are executed.
     *
     * @param sender
     * @param args
     */
    @Command(value = "uninstall", async = true, minSize = 1, description = "help.entries.uninstall", permission = "bukkitinstall.uninstall", aliases = {"remove"})
    public void uninstall(CommandSender sender, ArgumentParser args) {
        Uninstaller uninstaller = new Uninstaller(new CommandSenderReceiver(sender), plugin.getPluginList());
        sender.sendMessage(Headers.getHeader(sender, _(sender, "uninstall.title")));
        try {
            uninstaller.uninstallPlugins(args.getArguments(0));
        } catch (FailedExecutionException e) {
            sender.sendMessage(ChatColor.RED + e.getMessage());
        }
    }

    /**
     * Checks for new updates...
     *
     * @param sender
     * @param arguments
     */
    @Command(value = "check-updates", permission = "bukkitinstall.check", description = "help.entries.check-updates", async = true, aliases = {"updates"})
    public void checkUpdates(CommandSender sender, ArgumentParser arguments) {
        Checker checker = new Checker(plugin.getPluginList());
        sender.sendMessage(Headers.getHeader(sender, _(sender, "check.title")));
        List<String> toUpdate;
        if (arguments.count() == 0) {
            toUpdate = checker.getUpdates();
        } else {
            toUpdate = checker.getUpdates(arguments.getArguments(0));
        }

        if (toUpdate.isEmpty())
            toUpdate.add(_(sender, "check.none"));

        sender.sendMessage(_(sender, "check.prefix", StringUtils.join(toUpdate, _(sender, "check.separator"))));
    }

}
