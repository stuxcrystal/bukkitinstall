/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.commands;

import net.stuxcrystal.bukkitinstall.BukkitInstallPlugin;
import net.stuxcrystal.bukkitinstall.commandhandler.ArgumentParser;
import net.stuxcrystal.bukkitinstall.commandhandler.Command;
import net.stuxcrystal.bukkitinstall.l10n.Headers;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import static net.stuxcrystal.bukkitinstall.l10n.Translations._;

/**
 * Commands to handle the configuration of BukkitInstall
 */
public class ConfigurationCommands {

    private BukkitInstallPlugin plugin;

    public ConfigurationCommands() {
        this.plugin = BukkitInstallPlugin.getInstance();
    }

    @Command(value = "", permission = "bukkitinstall.config")
    public void base(CommandSender sender, ArgumentParser parser) {
        sender.sendMessage(Headers.getHeader(sender, _(sender, "configuration.help.title")));
        sender.sendMessage(_(sender, "configuration.help.reload"));
        sender.sendMessage(_(sender, "configuration.help.reset"));
    }

    @Command(value = "reload", maxSize = 0, minSize = 0, permission = "bukkitinstall.config.reload")
    public void reload(CommandSender sender, ArgumentParser parser) {
        this.plugin.getConfiguration().reloadConfiguration();
        sender.sendMessage(_(sender, "configuration.reload"));
    }

    @Command(value = "reset", maxSize = 0, minSize = 0, permission = "bukkitinstall.config.reset")
    public void reset(CommandSender sender, ArgumentParser parser) {
        this.plugin.getConfiguration().saveDefaultConfiguration();
        this.plugin.getConfiguration().reloadConfiguration();
        sender.sendMessage(_(sender, "configuration.reset"));
    }

}
