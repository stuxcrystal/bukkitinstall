/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.commands;

import java.io.IOException;
import java.util.List;

import net.stuxcrystal.bukkitinstall.MessageReceiver;
import net.stuxcrystal.bukkitinstall.executors.MigrateExecutor;
import net.stuxcrystal.bukkitinstall.l10n.Headers;
import net.stuxcrystal.bukkitinstall.utils.CommandSenderReceiver;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.stuxcrystal.bukkitinstall.BukkitInstallPlugin;
import net.stuxcrystal.bukkitinstall.commandhandler.ArgumentParser;
import net.stuxcrystal.bukkitinstall.commandhandler.Command;
import net.stuxcrystal.bukkitinstall.commandhandler.SubCommand;

import static net.stuxcrystal.bukkitinstall.l10n.Translations._;

public class AdminCommands {

    private static int MAX_PAGE_SIZE = 5;

    private BukkitInstallPlugin plugin;

    public AdminCommands(BukkitInstallPlugin plugin) {
        this.plugin = plugin;
    }

    @Command(value = "", permission = "bukkitinstall.use")
    public void welcome(CommandSender sender, ArgumentParser arguments) {
        command(sender, arguments);
        sender.sendMessage("");
        sender.sendMessage(_(sender, "about.usage.help"));
    }

    @Command(value = "version", permission = "bukkitinstall.version", description = "help.entries.version")
    public void command(CommandSender sender, ArgumentParser arguments) {
        String version = plugin.getDescription().getVersion();
        String name = plugin.getDescription().getName();
        String authors = StringUtils.join(plugin.getDescription().getAuthors(), _(sender, "about.version.authors.separator"));

        sender.sendMessage(_(sender, "about.version.versioninfo", name, version));
        sender.sendMessage(_(sender, "about.version.authors.text", authors));
    }

    @Command(value = "help", permission = "bukkitinstall.help", description = "help.entries.help", aliases = {"/h", "-h", "--help"})
    public void help(CommandSender sender, ArgumentParser arguments) {
        List<Command> commands = plugin.getCommandHandler().getDescriptors();
        for (int i = 0; i < commands.size(); i++) {
            if (commands.get(i).value().isEmpty()) {
                commands.remove(i);
                break;
            }
        }

        int pages = (int) Math.ceil(commands.size() / (double) MAX_PAGE_SIZE);
        int position = Math.min((arguments.count() > 0 ? NumberUtils.toInt(arguments.getString(0), 1) : 1), pages) - 1;

        int currentId;
        sender.sendMessage(Headers.getHeaderPaginized(sender, _(sender, "help.title"), position+1, pages));
        for (int i = 0; i < MAX_PAGE_SIZE; i++) {
            currentId = position * MAX_PAGE_SIZE + i;

            if (currentId >= commands.size())
                break;

            Command command = commands.get(currentId);
            sender.sendMessage(_(sender, "help.entry", command.value(), _(sender, "help.description." + command.value())));
        }

        if (pages > 1)
            sender.sendMessage(_(sender, "help.next-page"));
    }

    @SubCommand(MetricsCommand.class)
    @Command(value = "metrics", permission = "bukkitinstall.metrics", description = "help.entries.metrics", aliases = {"pm"})
    public void controlMetrics(CommandSender sender, ArgumentParser parser) {}

    @SubCommand(ConfigurationCommands.class)
    @Command(value = "config", permission = "bukkitinstall.config", description = "help.entries.config", aliases = {"configuration"})
    public void configuration(CommandSender sender, ArgumentParser parser) {}


    @Command(value = "migrate", permission = "bukkitinstall.migrate", description = "help.entries.migrate", async = true, minSize = 1)
    public void migrateUpdater(CommandSender sender, ArgumentParser parser) {
        MigrateExecutor migrator = MigrateExecutor.getInstance();
        MessageReceiver receiver = new CommandSenderReceiver(sender);

        try {
            migrator.migrate(receiver, parser.getString(0), parser.getArguments(1));
        } catch (IOException e) {
            e.printStackTrace();
            sender.sendMessage(_(sender, "migrations.fail"));
            return;
        }

        sender.sendMessage(_(sender, "migrations.success"));
    }
}
