/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

// Copyright (c) 2013 StuxCrystal
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
package net.stuxcrystal.bukkitinstall.commandhandler;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * A manager for translations.
 *
 * @author StuxCrystal
 */
public class TranslationManager {

    /**
     * The manager that is handling the translations
     */
    private static final TranslationManager INSTANCE = new TranslationManager();

    /**
     * The handler that is handling the translations.
     */
    private TranslationHandler handler = null;

    /**
     * Represents the default translations of the system.
     */
    private static Map<String, String> defaults = new HashMap<String, String>();

    /**
     * Default Translations for Command-Fails.
     */
    static {
        defaults.put("cmd.notfound", ChatColor.RED + "Command not found.");
        defaults.put("cmd.exception", ChatColor.RED + "An exception occured while executing the command.");
        defaults.put("cmd.call.fail", ChatColor.RED + "Failed to call command.");
        defaults.put("cmd.check.noplayer", ChatColor.RED + "This command cannot be executed by a player.");
        defaults.put("cmd.check.noconsole", ChatColor.RED + "This command cannot be executed from the console.");
        defaults.put("cmd.check.permission", ChatColor.RED + "You don't have permission.");
        defaults.put("cmd.check.flag", ChatColor.RED + "Invalid flag.");
        defaults.put("cmd.check.args.min", ChatColor.RED + "Too few arguments.");
        defaults.put("cmd.check.args.max", ChatColor.RED + "Too many arguments.");
    }

    /**
     * Private Constructor to ensure that no Translations overrides another translation.
     */
    private TranslationManager() {
    }

    /**
     * Translates a given text
     *
     * @param sender
     * @param key
     * @return
     */
    public String translate(CommandSender sender, String key) {
        String result = null;
        if (handler == null || (result = handler.getTranslation(sender, key)) == null) {
            result = defaults.get(key);
        }
        if (result == null)
            result = key;

        return result;
    }

    public void setHandler(TranslationHandler handler) {
        this.handler = handler;
    }

    /**
     * Gets the Translations-Instance.
     *
     * @return
     */
    public static TranslationManager getInstance() {
        return INSTANCE;
    }

    /**
     * Translator
     *
     * @param sender The sender.
     * @param key
     * @return
     */
    public static String _(CommandSender sender, String key) {
        return getInstance().translate(sender, key);
    }

}
