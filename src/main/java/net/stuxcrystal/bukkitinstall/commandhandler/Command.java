/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

// Copyright (c) 2013 StuxCrystal
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
package net.stuxcrystal.bukkitinstall.commandhandler;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;
import java.lang.annotation.Documented;

/**
 * Defines a command.
 *
 * @author StuxCrystal
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Command {

    /**
     * The name of the command.
     *
     * @return
     */
    public String value();

    /**
     * The aliases of the command.
     *
     * @return
     */
    public String[] aliases() default {};

    /**
     * The permission needed to execute.<br>
     * Default: None
     *
     * @return
     */
    public String permission() default "";

    /**
     * The description of the command.
     *
     * @return
     */
    public String description() default "";

    /**
     * Can the command be executed as a player.<br>
     * Default: true
     *
     * @return
     */
    public boolean asConsole() default true;

    /**
     * Can the command be executed as a player.<br>
     * Default: true
     *
     * @return
     */
    public boolean asPlayer() default true;

    /**
     * Should the command be execute asynchronous.
     *
     * @return
     */
    public boolean async() default false;

    /**
     * All supported flags.
     *
     * @return
     */
    public String flags() default "";

    /**
     * The minimal count of arguments needed.
     *
     * @return
     */
    public int minSize() default -1;

    /**
     * The maximal count of arguments needed.
     *
     * @return
     */
    public int maxSize() default -1;

}
