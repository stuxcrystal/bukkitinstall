/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

// Copyright (c) 2013 StuxCrystal
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
package net.stuxcrystal.bukkitinstall.commandhandler;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import static net.stuxcrystal.bukkitinstall.commandhandler.TranslationManager._;

import org.apache.commons.lang.ArrayUtils;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * The handler for commands.
 *
 * @author StuxCrystal
 */
public class CommandHandler implements CommandExecutor {

    /**
     * The internal datastructure.
     *
     * @author StuxCrystal
     */
    static class CommandData {

        /**
         * The Command-Annotation.
         */
        private Command command;

        /**
         * Subcommand handler.
         */
        private CommandHandler subcommands;

        /**
         * The Method.
         */
        private Method method;

        /**
         * The Instance used with the method.
         */
        private Object instance;

        /**
         * The JavaPlugin for the logs.
         */
        private JavaPlugin plugin;

        public CommandData(Command command, Method method, Object instance, JavaPlugin plugin, CommandHandler subcommands) {
            this.command = command;
            this.method = method;
            this.instance = instance;
            this.plugin = plugin;
            this.subcommands = subcommands;
        }

        /**
         * Executes the command.<p />
         * Calls the subcommand if needed.
         *
         * @param sender
         * @param arguments
         */
        public void execute(CommandSender sender, ArgumentParser arguments) {
            if (this.subcommands == null || this.subcommands.getSubCommand().time() == CallTime.PRE)
                if (!_execute(sender, arguments)) {
                    if (this.subcommands == null)
                        sender.sendMessage(_(sender, "cmd.notfound"));
                    return;
                }

            if (this.subcommands == null)
                return;

            // Parse the command list.
            SubCommand command = this.subcommands.getSubCommand();
            String name = null;
            String[] args = null;

            // Parse the command.
            if (arguments.count() == 0) {
                // An empty name represents a call without an argument
                name = "";
                args = new String[0];
            } else if (arguments.count() == 1) {
                name = arguments.getString(0);
                args = new String[0];
            } else {
                name = arguments.getString(0);
                args = arguments.getArguments(1);
            }

            // Executes the subcommand.
            if (!this.subcommands.execute(sender, name, args) && command.time() == CallTime.FALLBACK)
                if (!_execute(sender, arguments))
                    sender.sendMessage(_(sender, "cmd.notfound"));

            // Call the arguments.
            if (command.time() == CallTime.POST)
                _execute(sender, arguments);
        }

        /**
         * Invokes the command.
         *
         * @param sender
         * @param arguments
         * @return
         */
        private boolean _execute(CommandSender sender, ArgumentParser arguments) {
            try {
                this.method.invoke(this.instance, new Object[]{sender, arguments});
            } catch (IllegalAccessException | IllegalArgumentException e) {
                sender.sendMessage(_(sender, "cmd.call.fail"));
                plugin.getLogger().log(Level.WARNING, "Failed to execute command.", e);
            } catch (InvocationTargetException e) {
                if (e.getCause() instanceof DoNotExecuteException) {
                    return false; // Don't execute the subcommand.
                }
                sender.sendMessage(_(sender, "cmd.exception"));
                plugin.getLogger().log(Level.SEVERE, "Exception while calling command.", e.getCause());
            }

            return true;
        }
    }

    /**
     * List of commands.
     */
    private List<CommandData> commands = new ArrayList<CommandData>();

    /**
     * The java plugin to register tasks.
     */
    private JavaPlugin plugin;

    /**
     * Data for the subcommand.
     */
    private SubCommand subcommand = null;

    /**
     * The Constructor for base-commands.
     *
     * @param plugin
     */
    public CommandHandler(JavaPlugin plugin) {
        this(plugin, null);
    }

    /**
     * Internal constructor for subcommand-support.
     *
     * @param plugin
     * @param subcommand
     */
    private CommandHandler(JavaPlugin plugin, SubCommand subcommand) {
        this.plugin = plugin;
        this.subcommand = subcommand;
    }

    /**
     * Registers the commands.<p />
     * Also prepares subcommands.
     *
     * @param container
     */
    public void registerCommands(Object container) {
        for (Method method : container.getClass().getDeclaredMethods()) {

            // The method has to be accessible.
            if (!method.isAccessible()) {
                method.setAccessible(true);
            }

            // The method have to be annotated by Command.
            if (!method.isAnnotationPresent(Command.class)) continue;

            CommandHandler subhandler = null;
            if (method.isAnnotationPresent(SubCommand.class)) {
                SubCommand command = method.getAnnotation(SubCommand.class);
                subhandler = new CommandHandler(this.plugin, command);

                Class<?>[] classes = command.value();
                Object current;
                for (Class<?> cls : classes) {
                    current = newInstance(cls);
                    if (current != null)
                        subhandler.registerCommands(newInstance(cls));
                }
            }

            // Add the command.
            commands.add(new CommandData(method.getAnnotation(Command.class), method, container, this.plugin, subhandler));
        }
    }

    /**
     * Registers commands using classes. The constructor must not have any arguments.
     *
     * @param container The class that contains the methods.
     */
    public void registerCommands(Class<?> container) {
        Object o = newInstance(container);
        this.registerCommands(o);
    }

    /**
     * Constructs a new instance.
     *
     * @param cls The class to construct.
     * @return A new object.
     */
    private Object newInstance(Class<?> cls) {
        try {
            return cls.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Executes the command.<p />
     * Searches the command.
     *
     * @param sender    The sender that executes the command.
     * @param name      The name of the command.
     * @param arguments The arguments.
     * @return false if the command couldn't be found.
     */
    public boolean execute(CommandSender sender, String name, String[] arguments) {
        // Prefer Exact Matches first.
        for (CommandData data : commands) {
            if (data.command.value().equals(name)) {
                executeCommand(sender, data, arguments);
                return true;
            }
        }

        // Then ignore the case.
        for (CommandData data : commands) {
            if (data.command.value().equalsIgnoreCase(name)) {
                executeCommand(sender, data, arguments);
                return true;
            }
        }

        // Exact matches to aliases.
        for (CommandData data : commands) {
            for (String alias : data.command.aliases()) {
                if (alias.equals(name)) {
                    executeCommand(sender, data, arguments);
                    return true;
                }
            }
        }

        // Match aliases without caring for the case.
        for (CommandData data : commands) {
            for (String alias : data.command.aliases()) {
                if (alias.equalsIgnoreCase(name)) {
                    executeCommand(sender, data, arguments);
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Returns the name of the description.
     *
     * @param name
     * @return
     */
    public String getDescription(String name) {
        for (CommandData data : commands) {
            if (data.command.value().equals(name)) {
                return data.command.description();
            }
        }

        return null;
    }

    /**
     * Returns a lift of all descriptors.
     *
     * @return
     */
    public List<Command> getDescriptors() {
        List<Command> commands = new ArrayList<Command>();

        for (CommandData data : this.commands) {
            commands.add(data.command);
        }

        return commands;
    }

    /**
     * Executes the command.
     *
     * @param sender    The sender
     * @param data      The internal data of the command.
     * @param arguments The arguments.
     */
    private void executeCommand(CommandSender sender, CommandData data, String[] arguments) {
        // Check sender type.
        if (sender instanceof Player) {
            if (!data.command.asPlayer()) {
                sender.sendMessage(_(sender, "cmd.check.noplayer"));
                return;
            }
        } else {
            if (!data.command.asConsole()) {
                sender.sendMessage(_(sender, "cmd.check.noconsole"));
                return;
            }
        }

        // Check permissions.
        if (!data.command.permission().isEmpty() && !sender.hasPermission(data.command.permission())) {
            sender.sendMessage(_(sender, "cmd.check.permission"));
            return;
        }

        // Check argument data.
        ArgumentParser parser = new ArgumentParser(arguments);

        // Check if only these flags are in the flag list
        if (!data.command.flags().isEmpty() && !parser.getFlags().matches("[" + data.command.flags() + "]*")) {
            sender.sendMessage(_(sender, "cmd.check.flag"));
            return;
        }

        if (data.command.minSize() != -1 && parser.count() < data.command.minSize()) {
            sender.sendMessage(_(sender, "cmd.check.args.min"));
            return;
        }

        if (data.command.maxSize() != -1 && parser.count() > data.command.maxSize()) {
            sender.sendMessage(_(sender, "cmd.check.args.max"));
            return;
        }

        // Execute Command.
        if (data.command.async())
            // Asynchronous Execution if Command.async is true
            plugin.getServer().getScheduler().runTaskAsynchronously(plugin, new CommandExecutionTask(data, sender, parser));
        else
            // Synchronous execution if Command.async is false.
            data.execute(sender, parser);
    }

    /**
     * Simple command handler for subcommands.
     */
    @Override
    public boolean onCommand(CommandSender sender, org.bukkit.command.Command command, String label, String[] arguments) {
        if (arguments.length == 0) {
            // Make a command out of it.
            arguments = new String[]{""};
        }

        if (!this.execute(sender, arguments[0], (String[]) ArrayUtils.remove(arguments, 0)))
            sender.sendMessage(_(sender, "cmd.notfound"));

        return true;
    }

    /**
     * Use this function to implement a command-switch for the plugin.
     *
     * @param sender    The sender.
     * @param command   The command
     * @param label     The label
     * @param arguments The arguments
     * @return
     */
    public boolean commandSwitch(CommandSender sender, org.bukkit.command.Command command, String label, String[] arguments) {
        if (!this.execute(sender, label, arguments))
            return false;

        return true;
    }

    /**
     * If this handler represents a command-handler, use this function.
     *
     * @return
     */
    public SubCommand getSubCommand() {
        return subcommand;
    }

}
