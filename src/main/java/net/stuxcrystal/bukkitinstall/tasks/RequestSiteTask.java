/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.tasks;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

import net.stuxcrystal.bukkitinstall.utils.IOUtil;

/**
 * The RequestSiteTask downloads a file and returns its content as a string.<p />
 *
 * This task can also add data to the request body.
 */
public class RequestSiteTask implements InstallTask<String> {

    private URL url;

    private String post = null;

    public RequestSiteTask(URL url) {
        this.url = url;
    }

    public RequestSiteTask(URL url, String post) {
        this.url = url;
        this.post = post;
    }

    @Override
    public String run() throws IOException {
        if (post == null) {
            InputStream stream = url.openStream();
            String data = IOUtil.convertStreamToString(stream);
            stream.close();
            return data;
        } else {
            URLConnection connection = url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Content-Length", "" + post.length());

            OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
            osw.write(post);
            osw.flush();

            InputStream is = connection.getInputStream();
            String data = IOUtil.convertStreamToString(is);
            is.close();

            return data;

        }
    }

}
