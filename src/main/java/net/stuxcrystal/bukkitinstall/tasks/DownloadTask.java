/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.tasks;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

/**
 * This task downloads a file.
 *
 * @author StuxCrystal
 */
public class DownloadTask implements InstallTask<Void> {

    private File file;

    private URL url;

    public DownloadTask(File file, URL url) {
        this.file = file;
        this.url = url;
    }

    @Override
    public Void run() throws IOException {
        URLConnection con = url.openConnection();
        return new StreamCopyTask(con.getInputStream(), file).run();
    }


}
