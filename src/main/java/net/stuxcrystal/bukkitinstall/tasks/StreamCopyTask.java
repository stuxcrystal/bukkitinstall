/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.tasks;

import net.stuxcrystal.bukkitinstall.BukkitInstallPlugin;
import net.stuxcrystal.bukkitinstall.ConfigurationFile;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.OpenOption;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.HashSet;

/**
 * Description
 *
 * @author stuxcrystal
 */
public class StreamCopyTask implements InstallTask<Void> {

    private InputStream is;

    private File f;

    public StreamCopyTask(InputStream is, File f) {
        this.is = is;
        this.f = f;
    }

    @Override
    public Void run() throws IOException {

        if (!f.exists()) {
            if (!f.getParentFile().exists()) {
                f.getParentFile().mkdirs();
            }

            f.createNewFile();
        }

        ConfigurationFile config = BukkitInstallPlugin.getInstance().getConfiguration();

        if (config.isUsingNIO()) {
            ReadableByteChannel channel = Channels.newChannel(is);
            FileChannel to = FileChannel.open(f.toPath(), new HashSet<OpenOption>(Arrays.asList(
                    StandardOpenOption.CREATE, StandardOpenOption.WRITE
            )));

            ByteBuffer buffer = ByteBuffer.allocateDirect(config.getBufferSize());

            while (channel.read(buffer) != -1) {
                buffer.flip();
                to.write(buffer);
                buffer.compact();
            }

            buffer.flip();

            while (buffer.hasRemaining())
                to.write(buffer);

            channel.close();
            to.close();

        } else {

            FileOutputStream fos = null;

            try {
                int size = config.getBufferSize();
                fos = new FileOutputStream(f);
                byte[] buffer = new byte[size];

                int transfer = 0;
                while ((transfer = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, transfer);
                }
            } finally {
                if (fos != null) fos.close();
                is.close();
            }
        }

        return null;
    }
}
