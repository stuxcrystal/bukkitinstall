/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.utils;

import org.bukkit.command.CommandSender;

import net.stuxcrystal.bukkitinstall.MessageReceiver;

import java.util.Locale;

public class CommandSenderReceiver implements MessageReceiver {

    private CommandSender sender;

    public CommandSenderReceiver(CommandSender sender) {
        this.sender = sender;
    }

    @Override
    public void sendMessage(String... message) {
        sender.sendMessage(message);
    }

    @Override
    public boolean hasPermission(String node) {
        return sender.hasPermission(node);
    }

    /**
     * The ip defined language is currently planned.
     * @return
     */
    @Override
    public Locale getLocale() {
        return Locale.getDefault();
    }

}
