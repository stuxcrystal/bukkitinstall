/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.utils;

import java.io.File;
import java.io.FilenameFilter;
import java.util.regex.Pattern;

public class PatternArrayNameFilter implements FilenameFilter {

    Pattern[] patterns;

    public PatternArrayNameFilter(Pattern[] patterns) {
        this.patterns = patterns;
    }

    @Override
    public boolean accept(File dir, String name) {
        for (Pattern pattern : this.patterns) {
            if (pattern.matcher(name).find())
                return true;
        }

        return false;
    }

}
