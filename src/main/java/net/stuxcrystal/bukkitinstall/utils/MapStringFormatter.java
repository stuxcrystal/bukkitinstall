/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.utils;

import org.apache.commons.lang.StringEscapeUtils;

import java.util.Map;
import java.util.regex.Pattern;

/**
 * Special formatter class for formatting strings.
 *
 * @author stuxcrystal
 */
public final class MapStringFormatter {

    public static final String format(String pre, Map<String, String> values) {
        String result = pre;
        for (Map.Entry<String, String> entry : values.entrySet()) {
            result = result.replaceAll("(?<!/)\\{" + regexEscape(entry.getKey()) + "\\}", entry.getValue());
        }

        return escapeResult(result);
    }

    private static final String regexEscape(String pre) {
        return Pattern.quote(pre.replace("}", "/}"));
    }

    private static final String escapeResult(String text) {
        return StringEscapeUtils.unescapeHtml(text);
    }

}
