/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.utils;

import net.stuxcrystal.bukkitinstall.BukkitInstallPlugin;

import java.lang.reflect.Field;

/**
 * Utilities for Reflections
 *
 * @author StuxCrystal
 */
public class ReflectionUtils {

    /**
     * Returns the object behind the field.
     *
     * @param o    The object.
     * @param path The path to the field.
     * @return null if the object coudn't be retrieved or casted; the casted object otherwise.
     */
    @SuppressWarnings("unchecked")
    public static <T> T getField(Object o, String... path) {
        Object current = o;
        for (String field : path) {
            Field f = getFieldObject(current.getClass(), field);
            if (field == null)
                return null;

            if (!f.isAccessible())
                f.setAccessible(true);

            try {
                current = f.get(current);
            } catch (IllegalArgumentException | IllegalAccessException e) {
                e.printStackTrace();
                return null;
            }
        }

        return (T) current;
    }

    /**
     * Returns the field of the class.
     *
     * @param cls  The class that declares the field.
     * @param name The name of the given field
     * @return The Field-Object or null if the field was not found.
     */
    private static Field getFieldObject(Class<?> cls, String name) {
        Class<?> current = cls;
        while (current != null) {
            if (current.equals(Object.class))
                break;

            try {
                return current.getDeclaredField(name);
            } catch (NoSuchFieldException | NoClassDefFoundError e) {
                current = cls.getSuperclass();
            }
        }

        return null;
    }

}
