/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.utils;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.NoSuchElementException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class IOUtil {
    /**
     * What a stupid scanner trick!<p />
     * Aka. Match the start. xD
     *
     * @param stream
     * @return
     */
    @SuppressWarnings("resource")
    public static String convertStreamToString(InputStream stream) {
        java.util.Scanner s = new java.util.Scanner(stream).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    /**
     * Returns the value under the given path.
     *
     * @param object
     * @param path
     * @return
     * @throws java.util.NoSuchElementException
     *
     */
    @SuppressWarnings("unchecked")
    public static <T> T getJSONValue(Object object, Object... path) throws NoSuchElementException {
        Object current = object;
        Object before = null;
        for (Object part : path) {
            if (part instanceof String) {
                current = ((JSONObject) current).get(part);
            } else if (part instanceof Integer) {
                current = ((JSONArray) current).get((int) part);
            } else if (part == null) {
                throw new NoSuchElementException("No such element: " + before);
            } else {
                throw new NoSuchElementException("Invalid type...");
            }

            before = part;
        }

        return (T) current;
    }

    /**
     * Encodes a map to a query.
     * @param data
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String encodeQuery(Map<String, String> data) throws UnsupportedEncodingException {
        StringBuilder sb = new StringBuilder();

        for (Map.Entry<String, String> entry : data.entrySet()) {
            if (sb.length()>0) {
                sb.append('&');
            }

            sb.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            sb.append("=");
            sb.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return sb.toString();
    }
}

