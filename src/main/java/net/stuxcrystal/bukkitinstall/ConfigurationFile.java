/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall;

import java.io.File;
import java.io.IOException;
import java.util.*;

import net.stuxcrystal.bukkitinstall.l10n.Translations;
import net.stuxcrystal.bukkitinstall.utils.MapStringFormatter;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.MemoryConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 * ConfigurationFile of BukkitInstall. Created for upcoming versions.
 *
 * @author StuxCrystal
 */
public class ConfigurationFile {

    /**
     * The configuration storing the data.
     */

    private MemoryConfiguration configuration = new YamlConfiguration();

    /**
     * Default configuration.
     */
    private Configuration defaults = new YamlConfiguration();

    /**
     * The file the configuration is saved in.
     */
    private File file;

    /**
     * A backreference to BukkitInstall for saving the default configuration.
     */
    private BukkitInstallPlugin plugin;

    /**
     * Loads the configuration using a file.
     * @param file The file to load.
     */
    public ConfigurationFile(BukkitInstallPlugin plugin, File file, Configuration def) {
        this.defaults = def;
        this.file = file;
        this.plugin = plugin;

        this.reloadConfiguration();
    }

    /**
     * Returns all databases of the plugin.
     * @return A list of names.
     */
    public List<String> getDatabases() {
        return this.configuration.getStringList("databases");
    }

    /**
     * List of experimental features to
     * @return
     */
    public List<String> getFeatures() {
        return this.configuration.getStringList("features");
    }

    /**
     * Returns the path to all plugin directories.
     * @return A list of valid paths.
     */
    public List<String> getPluginDirectories() {
        List<String> raw = this.configuration.getStringList("directories.directories");

        for (int i = 0; i<raw.size(); i++) {
            raw.set(i, this.parsePath(raw.get(i)));
        }

        return raw;
    }

    /**
     * Returns the default plugin directory.
     * @return A file object pointing to the default plugin directory.
     */
    public File getDefaultPluginDirectory() {
        int index = this.configuration.getInt("directories.default");
        return new File(getPluginDirectories().get(index));
    }

    /**
     * Returns a list of pathes that should be loaded by BukkitInstall.
     * @return A list of strings containing the pathes.
     */
    public List<String> getAutoLoadDirectories() {
        List<Integer> indexes = this.configuration.getIntegerList("directories.autoload");
        List<String> result = new ArrayList<String>(indexes.size());
        List<String> pathes = this.getPluginDirectories();

        for (int index : indexes) {
            result.add(pathes.get(index));
        }

        return result;
    }

    /**
     * Returns the directory where the language files are stored.
     * @return
     */
    public File getTranslationDirectory() {
        String directory = this.parsePath(this.configuration.getString("localization.directory"));
        return new File(directory);
    }

    /**
     * Returns the default language.
     * @return
     */
    public String getDefaultLanguage() {
        String locale = this.configuration.getString("localization.use");
        if ("none".equalsIgnoreCase(locale))
            return Locale.getDefault().getLanguage();
        return locale;
    }

    /**
     * Should the plugin use NIO.<p />
     *
     * Internal configuration. Don't touch.
     * @return true if the plugin should use NIO.
     */
    public boolean isUsingNIO() {
        return this.configuration.getBoolean("optimization.use-nio");
    }

    /**
     * The BufferSize to use.
     * @return
     */
    public int getBufferSize() {
        return this.configuration.getInt("optimization.buffer-size");
    }

    /**
     * Saves the Default-Configuration.
     */
    public void saveDefaultConfiguration() {
        this.plugin.saveResource("config.yml", true);

        if (this.plugin.getTranslator() != null) {
            try {
                this.plugin.getTranslator().prepare(true);
            } catch (IOException e) {
                this.plugin.getLogger().warning("Failed to save default translation file for the system language.");
                e.printStackTrace();
            }
        }
    }

    /**
     * Reloads the configuration.
     */
    public void reloadConfiguration() {
        this.configuration = YamlConfiguration.loadConfiguration(this.file);
        this.configuration.addDefaults(this.defaults);

        Translations trans = Translations.getInstance();
        if (trans != null)
            trans.reset();
    }

    /**
     * Parses the current path using the variables.
     * @param raw The raw configuration value.
     * @return the parsed path.
     */
    private String parsePath(String raw) {
        Map<String, String> str = new HashMap<String, String>();
        str.put("confdir", this.plugin.getDataFolder().toString());
        str.put("curdir", System.getProperty("user.dir"));
        str.put("home", System.getProperty("user.home"));

        return MapStringFormatter.format(raw, str);
    }
}
