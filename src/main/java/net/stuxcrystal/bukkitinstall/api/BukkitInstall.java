/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.api;

/**
 * The Bukkit-Install Interface.
 *
 * @author StuxCrystal
 */
public interface BukkitInstall {

    /**
     * Returns the {@link PluginList}.<p />
     *
     * The PluginList manages the plugin stored in your current plugin directory.
     */
    public PluginList getPluginList();

    /**
     * Returns the {@link PluginController}.<p />
     *
     * The PluginController contains the functions for installing and removing the plugins.
     */
    public PluginController getPluginController();

    /**
     * Returns the {@link PluginSearcher}.<p />
     *
     * The plugin searcher manages the Plugin-Databases.
     */
    public PluginSearcher getPluginSearcher();

    /**
     * Returns the {@link PluginMigrator}.<p />
     *
     * The PluginMigrator manages the migrate-feature of the plugin.
     * @return
     */
    public PluginMigrator getPluginMigrator();

}
