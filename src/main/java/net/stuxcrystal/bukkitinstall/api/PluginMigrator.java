/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.api;

import net.stuxcrystal.bukkitinstall.MessageReceiver;
import net.stuxcrystal.bukkitinstall.executors.MigrateExecutor;
import net.stuxcrystal.bukkitinstall.utils.CommandSenderReceiver;
import net.stuxcrystal.bukkitinstall.utils.NullReceiver;
import org.bukkit.command.CommandSender;

import java.io.IOException;

/**
 * The API-Class to migrate the updates of a plugin-updater into
 * BukkitInstall.
 */
public class PluginMigrator {

    /**
     * A instance of the NullReceiver.
     */
    private static final NullReceiver DEV_NULL = new NullReceiver();

    /**
     * Migrates the updates of a plugin-updater into the plugin-folder.
     *
     * @param receiver The message-receiver that receives the messages.
     * @param name The name of the migrators.
     * @param arguments The arguments the migrators receives
     * @return true if the migration process succeeded false otherwise.
     */
    public boolean migrate(MessageReceiver receiver, String name, String[] arguments) {
        MigrateExecutor executor =  MigrateExecutor.getInstance();

        if (receiver == null)
            receiver = DEV_NULL;

        try {
            executor.migrate(receiver, name, arguments);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Migrates the updates of a plugin-updater into the plugin-folder.
     *
     * @param sender The sender that receives the messages.
     * @param name The name of the migrator.
     * @param arguments The arguments the migrator receives.
     * @return true if the migration process succeeded, false otherwise.
     */
    public boolean migrate(CommandSender sender, String name, String[] arguments) {
        return this.migrate(new CommandSenderReceiver(sender), name, arguments);
    }

    /**
     * Migrates the updates of a plugin-updater into the plugin-folder.<p />
     *
     * Ignores any output.
     *
     * @param name
     * @param arguments
     * @return
     */
    public boolean migrate(String name, String[] arguments) {
        return this.migrate((MessageReceiver) null, name, arguments);
    }

    /**
     * Registers a migrators.
     * @param migrator The migrators-Instance.
     */
    public void registerMigrator(Migrator migrator) {
        MigrateExecutor.getInstance().registerMigrator(migrator);
    }

    /**
     * Removes a migrator.
     * @param name The name of the migrator.
     */
    public void removeMigrator(String name) {
        MigrateExecutor.getInstance().removeMigrator(name);
    }
}
