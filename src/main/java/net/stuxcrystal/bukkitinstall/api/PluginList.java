/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.api;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

import net.stuxcrystal.bukkitinstall.ConfigurationFile;
import net.stuxcrystal.bukkitinstall.executors.DependencyManager;
import net.stuxcrystal.bukkitinstall.utils.PatternArrayNameFilter;
import net.stuxcrystal.bukkitinstall.utils.ReflectionUtils;

import org.apache.commons.lang.ArrayUtils;
import org.bukkit.command.Command;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.plugin.*;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.PluginClassLoader;

/**
 * All plugin jars in the plugin directory.<p />
 * <p/>
 * Most functions are thread-safe and can be executed from another thread.<br>
 * Please note that the API-Functions regarding plugin-names as well as loading and unloading plugins should be
 * executed from the Bukkit-Thread because its using Bukkit-API methods that are not thread-safe.
 *
 * @author StuxCrystal
 */
public class PluginList {

    /**
     * The directory where the plugins are stored...
     */
    private ConfigurationFile config;

    /**
     * A reference to the plugin loader.
     */
    private PluginLoader loader;

    /**
     * The plugin manager of bukkit.
     */
    private PluginManager manager;

    /**
     * Instantiates the plugin list.
     *
     * @param config     The configuration containing the directory information.
     * @param loader     The loader that is loading the plugin
     * @param manager    The manager that manages the plugins.
     */
    public PluginList(ConfigurationFile config, PluginLoader loader, PluginManager manager) {
        this.config = config;
        this.loader = loader;
        this.manager = manager;
    }

    /**
     * Returns the default plugin directory.
     *
     * @return File file where the plugins are.
     */
    public File getPluginFolder() {
        return this.config.getDefaultPluginDirectory();
    }

    /**
     * Retrieves all PluginDescriptions in the plugin directory.
     *
     * @return A map containing all plugins and plugin descriptions.
     */
    public Map<File, PluginDescriptionFile> getExistingPlugins() {
        Map<File, PluginDescriptionFile> result = new HashMap<File, PluginDescriptionFile>();

        for (File file : getPluginFiles()) {
            PluginDescriptionFile description = getDescription(file);
            result.put(file, description);
        }

        return result;
    }

    /**
     * A quiet way of obtaining the plugin.yml
     *
     * @param file The file to be checked.
     * @return null if the file is not a valid plugin.
     */
    public PluginDescriptionFile getDescription(File file) {
        try {
            return loader.getPluginDescription(file);
        } catch (InvalidDescriptionException e) {
            return null;
        }
    }

    /**
     * Returns the PluginDescriptionFile of the Plugin behind
     *
     * @param name The name of the plugin.
     * @return null if the description file couldn't be obtained.
     */
    public PluginDescriptionFile getDescription(String name) {
        File file = this.getPluginFile(name);
        if (file == null)
            return null;
        return getDescription(file);
    }

    /**
     * Returns the file behind the plugin.<p />
     * <p/>
     * All Plugin-Classes implementing
     *
     * @param plugin The plugin which file should be returned.
     * @return The file of the plugin.
     * @throws IllegalArgumentException if the plugin is not a JavaPlugin.
     */
    public File getPluginFile(Plugin plugin) {

        // Search the file using the file-description-mapping if the plugin is not a JavaPlugin.
        if (!(plugin instanceof JavaPlugin)) {
            for (Entry<File, PluginDescriptionFile> entry : this.getExistingPlugins().entrySet()) {
                if (entry.getValue().getName().equalsIgnoreCase(plugin.getName()))
                    return entry.getKey();
            }

            return null;
        }

        return ReflectionUtils.getField(plugin, "file");
    }

    /**
     * Returns the plugin file with the given name.<p />
     * <p/>
     * If the plugin is already loaded this function will retrieve the file object using the plugin-instance. Otherwise
     * this function will check all plugins in the plugin directory.
     *
     * @param name The name to search.
     * @return the file of the plugin; null if the file couldn't be found.
     */
    public File getPluginFile(String name) {
        Plugin plugin = getPlugin(name);
        if (plugin == null) {
            for (Entry<File, PluginDescriptionFile> entry : this.getExistingPlugins().entrySet()) {
                if (entry.getValue().getName().equalsIgnoreCase(name)) {
                    return entry.getKey();
                }
            }

            return null;
        } else
            return getPluginFile(plugin);
    }

    /**
     * Returns true if the plugin could be found.
     *
     * @param name The name of the plugin.
     * @return true if the plugin exists.
     */
    public boolean exists(String name) {
        return getPluginFile(name) != null;
    }

    /**
     * Retrieves the Status of the plugin behind the file.
     *
     * @param file The file to be tested.
     * @return Returns a plugin-status representing the current status.
     */
    public PluginStatus getStatus(File file) {
        Plugin plugin = this.getPlugin(file);

        if (plugin == null)
            return PluginStatus.UNLOADED;
        else
            return this.manager.isPluginEnabled(plugin) ? PluginStatus.ACTIVATED : PluginStatus.DEACTIVATED;
    }

    /**
     * Retrieves a list of all existing plugin files.
     *
     * @return A list of all jar files in the plugin directory.
     */
    public File[] getPluginFiles() {
        File[] files = new File[0];

        for (String directory : this.config.getPluginDirectories()) {
            File dir = new File(directory);
            files = (File[]) ArrayUtils.addAll(
                files,
                dir.listFiles(new PatternArrayNameFilter(loader.getPluginFileFilters()))
            );
        }

        return files;
    }

    /**
     * Returns the plugin instance that was loaded by the given file.<p />
     * This function will return null, if the plugin is not loaded.
     *
     * @param file The file containing the plugin.
     * @return A plugin instance.
     */
    public Plugin getPlugin(File file) throws IllegalArgumentException {
        // Retrieve the name of the plugin.
        PluginDescriptionFile description = getDescription(file);
        String name = description.getName();

        return manager.getPlugin(name);
    }

    /**
     * Returns the plugin with the given name. Use this to get instances of plugins that are needed.
     *
     * @param name The name of the plugin
     * @return the casted plugin instance.
     */
    public Plugin getPlugin(String name) {
        return manager.getPlugin(name);
    }

    /**
     * Searches all plugins that depends on the given plugin.
     *
     * @param name the name of the plugin.
     * @return A list of file pointing to the dependency.
     */
    public List<File> getHardDependentPlugins(String name) {
        List<File> result = new ArrayList<File>();

        for (Entry<File, PluginDescriptionFile> description : this.getExistingPlugins().entrySet()) {
            // Don't check the plugin itself.
            if (description.getValue().getName().equalsIgnoreCase(name))
                continue;

            List<String> dependencies = description.getValue().getDepend();

            // If no dependencies are given ignore the dependency.
            if (dependencies == null)
                continue;

            for (String dependency : dependencies)
                if (dependency.equalsIgnoreCase(name)) {
                    result.add(description.getKey());
                    break;
                }
        }

        return result;
    }

    /**
     * Searches all plugins that depends on the given plugin.
     *
     * @param plugin The plugin where the returned plugins are dependent on.
     * @return A list of file pointing to the dependency.
     */
    public List<File> getHardDependentPlugins(Plugin plugin) {
        return getHardDependentPlugins(plugin.getName());
    }

    /**
     * Searches all plugins that depends on the given plugin.
     *
     * @param file The file descriptor of the plugin.
     * @return A list of file pointing to the dependency.
     */
    @SuppressWarnings("unchecked")
    public List<File> getHardDependentPlugins(File file) {
        String name = this.getName(file);
        return (List<File>) (name == null ? Collections.emptyList() : getHardDependentPlugins(name));
    }

    /**
     * Searches all plugins that depends on the given plugin.
     *
     * @param name The name of the plugin.
     * @return A list of file pointing to the dependency.
     */
    public List<File> getSoftDependentPlugins(String name) {
        List<File> result = new ArrayList<File>();

        for (Entry<File, PluginDescriptionFile> description : this.getExistingPlugins().entrySet()) {
            // Don't check the plugin itself.
            if (description.getValue().getName().equalsIgnoreCase(name))
                continue;

            List<String> dependencies = description.getValue().getSoftDepend();

            // If no dependencies are given ignore the dependency.
            if (dependencies == null)
                continue;

            for (String dependency : dependencies)
                if (dependency.equalsIgnoreCase(name)) {
                    result.add(description.getKey());
                    break;
                }
        }

        return result;
    }

    /**
     * Searches all plugins that depends on the given plugin.
     *
     * @param plugin The plugin where the returned plugins are dependent on.
     * @return A list of file pointing to the dependency.
     */
    public List<File> getSoftDependentPlugins(Plugin plugin) {
        return this.getSoftDependentPlugins(plugin.getName());
    }

    /**
     * Searches all plugins that depends on the given plugin.
     *
     * @param file The file pointing to the plugin.
     * @return A list of file pointing to the dependency.
     */
    @SuppressWarnings("unchecked")
    public List<File> getSoftDependentPlugins(File file) {
        String name = this.getName(file);
        return (List<File>) (name == null ? Collections.emptyList() : getSoftDependentPlugins(name));
    }

    /**
     * Returns all hard dependent plugins.<p />
     * This includes all dependent plugins of the dependent plugins.
     *
     * @param file The file of the plugin.
     * @return A list of plugin files.
     */
    public List<File> getAllHardDependentPlugins(File file) {
        return DependencyManager.getDependencies(file);
    }


    /**
     * Returns all hard dependent plugins.<p />
     * This includes all dependent plugins of the dependent plugins.
     *
     * @param plugin The plugin
     * @return A list of plugin files.
     */
    public List<File> getAllHardDependentPlugins(Plugin plugin) {
        return DependencyManager.getDependencies(plugin);
    }


    /**
     * Returns all hard dependent plugins.<p />
     * This includes all dependent plugins of the dependent plugins.
     *
     * @param name The name of the plugin.
     * @return A list of plugin files.
     */
    public List<File> getAllHardDependentPlugins(String name) {
        return DependencyManager.getDependencies(this.getPluginFile(name));
    }

    /**
     * Returns all dependencies of the plugin.<p />
     * <p/>
     * Please note that only the resolved dependencies will be added. If a dependency couldn't be resolved it won't be
     * added to the list.<p />
     * <p/>
     * If addSoftDependencies is true, soft-dependencies will be included too.
     *
     * @param file                The path to the jar-file of the plugin.
     * @param addSoftDependencies Also include soft-dependencies.
     * @return A list of files that have the dependency.
     * @throws IllegalArgumentException if the given file is not a plugin or file is null.
     */
    public List<File> getDependencies(File file, boolean addSoftDependencies) {
        if (file == null)
            throw new IllegalArgumentException("File is null.");

        Map<File, PluginDescriptionFile> plugins = this.getExistingPlugins();
        PluginDescriptionFile description = plugins.get(file);

        if (description == null)
            throw new IllegalArgumentException("The given file is not a plugin.");

        List<String> dependencies = description.getDepend();
        if (dependencies == null) {
            if (addSoftDependencies) {
                dependencies = new ArrayList<String>();
            } else {
                return Collections.EMPTY_LIST;
            }
        }

        if (addSoftDependencies) {
            List<String> soft = description.getSoftDepend();
            if (soft != null)
                dependencies.addAll(soft);
        }

        if (dependencies.isEmpty())
            return Collections.EMPTY_LIST;

        List<File> result = new ArrayList<File>();

        for (Entry<File, PluginDescriptionFile> entry : plugins.entrySet()) {
            if (dependencies.contains(entry.getValue().getName()))
                result.add(entry.getKey());
        }

        return result;
    }

    /**
     * Returns all dependencies of the plugin.<p />
     * <p/>
     * If addSoftDependencies is true, soft-dependencies will be added-
     *
     * @param plugin              The plugin which dependencies should be checked.
     * @param addSoftDependencies Should soft-dependencies be added.
     * @return A list of dependencies that are added.
     * @throws NoSuchElementException if the plugin-file couldn't be retrieved.
     */
    public List<File> getDependencies(Plugin plugin, boolean addSoftDependencies) {
        File file = this.getPluginFile(plugin);
        if (file == null)
            throw new NoSuchElementException("No such plugin found");

        return getDependencies(file, addSoftDependencies);

    }

    /**
     * Returns all dependencies of the given name of the plugin.<p />
     * <p/>
     * If addSoftDependencies is true, soft-dependencies will be added.
     *
     * @param name                The name of the plugin.
     * @param addSoftDependencies Should soft-dependencies be added.
     * @return A list of dependencies.
     * @throws NoSuchElementException if the plugin-file could not be retrieved.
     */
    public List<File> getDependencies(String name, boolean addSoftDependencies) {
        File file = this.getPluginFile(name);

        if (file == null)
            throw new NoSuchElementException("No such plugin found.");

        return getDependencies(file, addSoftDependencies);
    }

    /**
     * Returns all dependencies of the plugin.
     *
     * @param file The file containing the plugin.
     * @return A list of dependencies.
     */
    public List<File> getDependencies(File file) {
        return getDependencies(file, false);
    }

    /**
     * Returns all dependencies of the plugin-
     *
     * @param plugin The plugin which dependency should be returned.
     * @return
     */
    public List<File> getDependencies(Plugin plugin) {
        return getDependencies(plugin, false);
    }

    /**
     * Returns all dependencies of the plugin
     *
     * @param name The name of the plugin which dependencies should be returned
     * @return A list of dependencies.
     */
    public List<File> getDependencies(String name) {
        return getDependencies(name, false);
    }

    /**
     * Returns all dependencies of the plugin including dependencies of dependencies.<p />
     * <p/>
     * If includeSoftDependencies is true, soft-dependenices will also be included.
     *
     * @param file                    The file which dependencies should be returned.
     * @param includeSoftDependencies Should soft-dependencies be included.
     * @return A list of files of plugins.
     */
    public List<File> getAllDependencies(File file, boolean includeSoftDependencies) {
        List<File> oldDependencies = new ArrayList<File>();
        List<File> currentDependencies = new ArrayList<File>();
        List<File> newDependencies = null;

        currentDependencies.add(file);

        while (oldDependencies.size() != currentDependencies.size()) {
            for (File plugin : currentDependencies) {
                newDependencies = this.getDependencies(file, includeSoftDependencies);
                for (File dependency : newDependencies) {
                    if (!currentDependencies.contains(dependency)) {
                        currentDependencies.add(dependency);
                    }
                }
            }

            oldDependencies = new ArrayList<File>(currentDependencies);
        }

        return currentDependencies;
    }

    /**
     * Returns all dependencies of the plugin.<p />
     * <p/>
     * If includeSoftDependencies is true, soft-dependencies will be added to the list.
     *
     * @param plugin                  The plugin which dependencies should be returned.
     * @param includeSoftDependencies The soft-dependencies that should be added.
     * @return A list of files to plugins.
     */
    public List<File> getAllDependencies(Plugin plugin, boolean includeSoftDependencies) {
        File file = this.getPluginFile(plugin);
        if (file == null)
            throw new NoSuchElementException("Failed to receive plugin-file.");

        return this.getAllDependencies(file, includeSoftDependencies);
    }

    /**
     * Returns all dependencies of the plugin.<p />
     * <p/>
     * If includeSoftDependencies is true, soft-dependencies will be added to the list.
     *
     * @param name                    The name of the plugin which dependencies should be searched.
     * @param includeSoftDependencies Should soft-dependencies be added.
     * @return A list of files to plugins.
     */
    public List<File> getAllDependencies(String name, boolean includeSoftDependencies) {
        File file = this.getPluginFile(name);
        if (file == null)
            throw new NoSuchElementException("Plugin not found: " + name);
        return this.getAllDependencies(file, includeSoftDependencies);
    }

    /**
     * Returns all dependencies of the file without adding soft-dependencies.
     *
     * @param file The file to a plugin which dependencies should be calculated.
     * @return A list of files to plugins.
     */
    public List<File> getAllDependencies(File file) {
        return getAllDependencies(file, false);
    }

    /**
     * Returns all dependencies of the file without adding soft-dependencies.
     *
     * @param plugin The plugin which dependencies should be calculated.
     * @return A list of files to plugins
     */
    public List<File> getAllDependencies(Plugin plugin) {
        return getAllDependencies(plugin, false);
    }

    /**
     * Returns all dependencies of the file without adding soft-dependencies.
     *
     * @param name Name of the plugin which dependencies should be calculated.
     * @return A list of plugins.
     */
    public List<File> getAllDependencies(String name) {
        return getAllDependencies(name, false);
    }

    /**
     * Disables a plugin using its file pointer.
     *
     * @param file
     * @throws java.io.IOException If an IO-Operation failed.
     */
    public void disablePlugin(File file) {
        Plugin plugin = this.getPlugin(file);
        if (plugin == null) return;
        if (!this.manager.isPluginEnabled(plugin)) return;
        this.manager.disablePlugin(plugin);
    }

    /**
     * Enables a plugin using its file pointer.
     *
     * @param file The file pointing to the plugin.
     * @return true if the plugin was enabled successfully.
     */
    public boolean enablePlugin(File file) {
        Plugin plugin = this.getOrLoadPlugin(file);
        if (plugin == null) return false;
        if (this.manager.isPluginEnabled(plugin)) return true;
        this.manager.enablePlugin(plugin);
        return true;
    }

    /**
     * Checks if the plugins are the same...
     *
     * @param file   the plugin-file to be checked.
     * @param plugin the plugin to be checked.
     * @return true if the plugins are the same.
     */
    public boolean samePlugin(File file, Plugin plugin) {
        return this.getPluginFile(plugin).equals(file);
    }

    /**
     * Retrieves the name of the plugin pointed by the file.
     *
     * @param file The file object.
     * @return null if the file is not a plugin.
     */
    public String getName(File file) {
        PluginDescriptionFile pdf = this.getDescription(file);
        if (pdf == null)
            return null;
        return pdf.getName();
    }

    /**
     * Just returns the plugin or loads it.<p />
     * Return null if the load failed.
     *
     * @param file
     * @return
     */
    public Plugin getOrLoadPlugin(File file) {
        Plugin plugin = this.getPlugin(file);
        if (plugin == null) {
            try {
                return this.loadPlugin(file);
            } catch (UnknownDependencyException | InvalidPluginException | InvalidDescriptionException e) {
                return null;
            }
        }
        return plugin;
    }

    /**
     * Loads a plugin into Bukkit.<p />
     * A shorthand for {@code Bukkit.getPluginManager().loadPlugin(file);}
     *
     * @param file The file to the plugin.
     */
    public Plugin loadPlugin(File file) throws UnknownDependencyException, InvalidPluginException, InvalidDescriptionException {
        return this.manager.loadPlugin(file);
    }

    /**
     * Unloads a plugin using its file pointer.
     *
     * @param file the file to the plugin.
     */
    public void unloadPlugin(File file) {
        Plugin plugin = this.getPlugin(file);
        if (plugin == null) return;
        this.unloadPlugin(plugin);
    }

    /**
     * Unloads a plugin.
     *
     * @param plugin
     */
    public void unloadPlugin(Plugin plugin) {
        // Retrieve tables...
        Map<?, ?> lookup = ReflectionUtils.getField(this.manager, "lookupNames");
        List<?> plugins = ReflectionUtils.getField(this.manager, "plugins");

        SimpleCommandMap commands = ReflectionUtils.getField(this.manager, "commandMap");
        Map<String, Command> knownCommands = ReflectionUtils.getField(commands, "knownCommands");

        Map<String, PluginClassLoader> classLoaders = ReflectionUtils.getField(this.loader, "loaders0");

        if (lookup == null || plugins == null || commands == null || knownCommands == null || classLoaders == null) {
            throw new IllegalStateException("Failed to receive the required collections.");
        }

        // Disable plugin...
        if (this.manager.isPluginEnabled(plugin))
            this.manager.disablePlugin(plugin);

        // Removing from tables...
        synchronized (this.manager) {
            lookup.remove(plugin.getName());
            plugins.remove(plugin);
        }

        // Removing from commands...
        synchronized (commands) {
            Iterator<?> it = knownCommands.entrySet().iterator();
            while (it.hasNext()) {
                Entry<?, ?> entry = (Entry<?, ?>) it.next();
                if ((entry.getValue() instanceof PluginCommand)) {
                    PluginCommand c = (PluginCommand) entry.getValue();
                    if (c.getPlugin().getName().equalsIgnoreCase(plugin.getName())) {
                        c.unregister(commands);
                        it.remove();
                    }
                }
            }
        }
    }

    /**
     * Removes a plugin from the list and closes its plugin manager.
     *
     * @param plugin
     */
    public void hardUnloadPlugin(Plugin plugin) {
        PluginClassLoader loader = this.getClassLoader(plugin);
        this.unloadPlugin(plugin);
        try {
            loader.close();
        } catch (IOException e) {
        }
        ;
    }

    /**
     * Removes a plugin from the list and closes its plugin manager.
     *
     * @param plugin
     */
    public void hardUnloadPlugin(File plugin) {
        PluginClassLoader loader = this.getClassLoader(plugin);
        if (loader == null)
            return;
        this.unloadPlugin(plugin);
        try {
            loader.close();
        } catch (IOException e) {
        }
        ;
    }

    /**
     * Returns the Class-Loader for the plugin.
     *
     * @param file The file of the plugin.
     * @return The class loader or null if no class loader could be found.
     */
    public PluginClassLoader getClassLoader(File file) {
        Plugin plugin = this.getPlugin(file);
        if (plugin == null) return null;
        return this.getClassLoader(plugin);
    }

    /**
     * Returns the Class-Loader for the plugin.
     *
     * @param plugin the plugin which class loader is to return
     * @return The PluginClassLoader assigned to the plugin or null if no ClassLoader could be found.
     */
    public PluginClassLoader getClassLoader(Plugin plugin) {
        Map<String, PluginClassLoader> classLoaders = ReflectionUtils.getField(this.loader, "loaders0");
        return classLoaders.get(plugin.getDescription().getName());
    }

    /**
     * Returns all names of the plugins.
     *
     * @return
     */
    public String[] getPluginNames() {
        File[] files = this.getPluginFiles();
        String[] names = new String[files.length];

        for (int i = 0; i < names.length; i++) {
            names[i] = this.getName(files[i]);
        }

        return names;
    }

}
