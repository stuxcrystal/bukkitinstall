/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.api;

import net.stuxcrystal.bukkitinstall.ConfigurationFile;
import net.stuxcrystal.bukkitinstall.api.databases.PluginDatabase;
import net.stuxcrystal.bukkitinstall.api.databases.PluginDescription;

import java.io.IOException;
import java.util.*;

/**
 * This frontend provides access to the PluginDatabase-API.
 *
 * @author StuxCrystal
 */
public class PluginSearcher {

    /**
     * The registered databases.
     */
    private ConfigurationFile config;

    /**
     * Handles all databases.
     */
    private Map<String, PluginDatabase> databases = new HashMap<String, PluginDatabase>();

    /**
     * The plugin loader.
     * @param config The configuration-file.
     */
    public PluginSearcher(ConfigurationFile config) {
        this.config = config;
    }

    /**
     * Registers the database.<p />
     *
     * Please note that you should unhook your database when the plugin unloads.
     * @param database The database to install.
     */
    public void registerDatabase(PluginDatabase database) {
        this.databases.put(database.getName().toLowerCase(), database);
    }

    /**
     * Unregisters the database<p />
     *
     * Use this method when you unload the plugin.
     * @param database
     */
    public void unregisterDatabase(String database) {
        this.databases.remove(database);
    }

    /**
     * Returns all databases that are used by this plugin.
     * @return A list of databases.
     */
    public List<PluginDatabase> getDatabases() {
        List<PluginDatabase> databases = new ArrayList<PluginDatabase>();
        for (String name : this.config.getDatabases()) {
            PluginDatabase current = this.databases.get(name.toLowerCase());
            if (current!=null) databases.add(current);
        }
        return databases;
    }

    /**
     * Downloads the PluginDescription for the given name.
     * @param name The name of the plugin.
     * @return A PluginDescription if a plugin was found or null if no plugin was found.
     */
    public PluginDescription getDescription(String name) {
        return this.getDescription(name, null);
    }

    /**
     * Downloads the PluginDescription for the given name.
     * @param name The name of the plugin.
     * @param version The version of the plugin.
     * @return A PluginDescription if a plugin was found or null if no plugin was found.
     */
    public PluginDescription getDescription(String name, String version) {
        PluginDescription result = null;

        for (PluginDatabase database : this.getDatabases()) {
            try {
                if (version != null)
                    result = database.getPlugin(name, version);
                else
                    result = database.getPlugin(name);
            } catch (IOException e) {
                continue;
            }

            if (result != null) break;
        }

        return result;
    }

    /**
     * Searches for a plugin.
     * @param query
     * @param site
     * @param length
     * @return
     */
    public List<PluginDescription> searchPlugins(String query, int site, int length) {
        List<PluginDescription> descriptions = new ArrayList<PluginDescription>();

        for (PluginDatabase database : this.getDatabases()) {
            List<PluginDescription> results;
            try {
                results = database.searchPlugins(query);
            } catch (IOException e) {
                continue;
            }

            if (results == null || results.isEmpty())
                continue;


            descriptions.addAll(results);
        }

        List<PluginDescription> result = new ArrayList<PluginDescription>(length);
        for (int i = site*length; i<descriptions.size(); i++) {
            result.add(descriptions.get(i));
        }
        return result;

    }

}
