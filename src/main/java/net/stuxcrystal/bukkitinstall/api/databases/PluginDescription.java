package net.stuxcrystal.bukkitinstall.api.databases;

import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Plugin-Description defines information about a plugin.<p />
 *
 * This class is completely serializable in Bukkit and using the storage APIs of Java.
 *
 * @author StuxCrystal
 */
public class PluginDescription implements Serializable, ConfigurationSerializable {

    /**
     * The Serial-Version UID will be updated if the file changed.<p />
     * <p/>
     * The UID states the last version the implementation changed.<br />
     * {@code (Major.Version+1)*1,000,000 + Minor.Version*100,000 + Fix.Version*10,000 + Build <p />
     *
     * Last Change in version: 0.2.0.0002
     */
    private static final long serialVersionUID = 1200002;

    /**
     * The name of the plugin.
     */
    private String name = null;

    /**
     * The description of the plugin.
     */
    private String description = null;

    /**
     * The download-link of the plugin.
     */
    private String download = null;

    /**
     * The filename containing of the plugin.
     */
    private String fileName = null;

    /**
     * The version of the plugin.
     */
    private String version = null;

    /**
     * The Site-URL of the plugin.
     */
    private String siteUrl = null;

    /**
     * A list of authors.
     */
    private List<String> authors = null;

    /**
     * Bean implementation of PluginDescription.
     */
    public PluginDescription() {}

    /**
     * Instantiates a PluginDescription with its values.
     *
     * @param name        The name of the plugin.
     * @param description The description of the plugin
     * @param download    The download-link of the plugin.
     * @param filename    The name of the file that will be downloaded.
     * @param version     The version of the plugin.
     * @param siteUrl     The URL of the plugin-page.
     * @param authors     The authors of the plugin.
     */
    public PluginDescription(String name, String description, String download, String filename, String version, String siteUrl, List<String> authors) {
        this.name = name;
        this.description = description;
        this.download = download;
        this.version = version;
        this.siteUrl = siteUrl;
        this.authors = authors;
        this.fileName = filename;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDownload() {
        return download;
    }

    public void setDownload(String download) {
        this.download = download;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getSiteUrl() {
        return siteUrl;
    }

    public void setSiteUrl(String siteUrl) {
        this.siteUrl = siteUrl;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Serializes the Object into a map that Bukkit can serialize.
     *
     * @return A map of strings.
     */
    public Map<String, Object> serialize() {
        Map<String, Object> result = new HashMap<String, Object>();

        result.put("name", name);
        result.put("desc", description);
        result.put("dl", download);
        result.put("filename", fileName);
        result.put("version", version);
        result.put("siteurl", siteUrl);
        result.put("authors", authors);

        return result;
    }

}
