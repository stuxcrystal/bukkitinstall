package net.stuxcrystal.bukkitinstall.api.databases;

import java.io.IOException;
import java.util.List;

/**
 * All implementations of this class provides access to a Plugin-Database.<p />
 * <p/>
 * The configuration will handle when the PluginDatabase should be called.
 *
 * @author StuxCrystal
 */
public interface PluginDatabase {

    /**
     * Each Plugin-Database must have a name.
     *
     * @return The name of the implementation.
     */
    public String getName();

    /**
     * Downloads the Meta-Data of the plugin with the latest from the Plugin-Database.<p />
     *
     * If the plugin was not found, this method returns null.
     *
     * @param name The name of the plugin. Case insensitive.
     * @return A plugin-description.
     * @throws IOException If an I/O-Operation fails.
     */
    public PluginDescription getPlugin(String name) throws IOException;

    /**
     * Downloads the Meta-Data of the plugin with the given version from the Plugin-Database<p />
     *
     * If the plugin was not found, this method returns null.
     *
     * @param name the name of the plugin. Case insensitive.
     * @param version the version of the plugin
     * @return A plugin-description or null if the plugin was not found.
     * @throws IOException If an I/O-Operation fails.
     */
    public PluginDescription getPlugin(String name, String version) throws IOException;

    /**
     * Searches for a plugin.
     * @param search The query
     * @return A list of PluginDescriptions or null if the database does not support searching.
     * @throws IOException if an I/O-Operation fails.
     */
    public List<PluginDescription> searchPlugins(String search) throws IOException;

}
