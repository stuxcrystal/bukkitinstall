/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.api;

import net.stuxcrystal.bukkitinstall.MessageReceiver;

import java.io.IOException;

/**
 * This class stores all migrators.<p />
 *
 * A migrator copies all plugins from an updater-plugin like CraftBukkitUpToDateMigrator and
 * migrates its update files into the plugin directory.<p />
 *
 *
 */
public interface Migrator {

    /**
     * The name of the migrator.
     */
    public String getName();

    /**
     * When a migrator should be executed,
     * @param sender The sender that sends the request. (For permission-checking)
     * @param arguments The arguments the player has added.
     */
    public void execute(MessageReceiver sender, String[] arguments) throws IOException;


}
