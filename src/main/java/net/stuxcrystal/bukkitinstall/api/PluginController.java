/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall.api;

import net.stuxcrystal.bukkitinstall.BukkitInstallPlugin;
import net.stuxcrystal.bukkitinstall.FailedExecutionException;
import net.stuxcrystal.bukkitinstall.MessageReceiver;
import net.stuxcrystal.bukkitinstall.executors.Installer;
import net.stuxcrystal.bukkitinstall.executors.MigrateExecutor;
import net.stuxcrystal.bukkitinstall.executors.Uninstaller;
import net.stuxcrystal.bukkitinstall.utils.CommandSenderReceiver;
import net.stuxcrystal.bukkitinstall.utils.NullReceiver;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.io.IOException;

/**
 * The API for Plugins to install or remove other plugins as well as executing a migrate command.
 *
 * @author StuxCrystal
 */
public class PluginController {

    /**
     * A instance of the NullReceiver.
     */
    private static final NullReceiver DEV_NULL = new NullReceiver();

    /**
     * A reference to BukkitInstall
     */
    private BukkitInstallPlugin install;

    /**
     * The plugin-controller to use.
     *
     * @param install
     */
    public PluginController(BukkitInstallPlugin install) {
        this.install = install;
    }

    /**
     * Installs plugins.
     *
     * @param sender     The sender where the output should be returned. If null, no output will be returned.
     * @param softDepend Installs soft-dependencies too.
     * @param update     Force the update of all plugins.
     * @param plugins    All plugins to install. This will include plugins that the plugins depends on.
     */
    public void installPlugins(CommandSender sender, boolean softDepend, boolean update, String... plugins) {
        MessageReceiver receiver = null;
        if (sender == null) {
            receiver = DEV_NULL;
        } else {
            receiver = new CommandSenderReceiver(sender);
        }

        Installer installer = new Installer(receiver, install.getPluginList(), install.getPluginSearcher());
        installer.setUseSoftDepend(softDepend);
        installer.setForceUpdate(update);
        installer.installPlugins(plugins);
    }

    /**
     * Installs plugins without updating existing ones.
     *
     * @param sender     The sender where the output should be returned. If null, no output will be returned.
     * @param softDepend Installs soft-dependencies too.
     * @param plugins    All plugins to install. This will include plugins that the plugins depends on.
     */
    public void installPlugins(CommandSender sender, boolean softDepend, String... plugins) {
        installPlugins(sender, softDepend, false, plugins);
    }

    /**
     * Installs plugins without updating existing ones and without installing soft-dependencies.
     *
     * @param sender  The sender where the output should be returned. If null, no output will be returned.
     * @param plugins All plugins to install. This will include plugins that the plugins depends on.
     */
    public void installPlugins(CommandSender sender, String... plugins) {
        installPlugins(sender, false, false, plugins);
    }

    /**
     * Installs plugins without giving an output and without updating the exisitng ones.
     *
     * @param softDepend
     * @param plugins
     */
    public void installPlugins(boolean softDepend, String... plugins) {
        installPlugins(null, softDepend, false, plugins);
    }

    /**
     * Install plugins without giving an output and without updating existing ones and without installing soft-dependencies.
     *
     * @param plugins The plugins to install
     */
    public void installPlugins(String... plugins) {
        installPlugins(null, false, false, plugins);
    }

    /**
     * Installs plugins.<p />
     * This will update existing plugins too.
     *
     * @param sender     The sender where the output should be returned. If null, no output will be returned.
     * @param softDepend Installs soft-dependencies too.
     * @param plugins    All plugins to install. This will include plugins that the plugins depends on.
     */
    public void updatePlugins(CommandSender sender, boolean softDepend, String... plugins) {
        installPlugins(sender, softDepend, true, plugins);
    }

    /**
     * Installs plugins and updating existing ones but without installing/updating soft-dependencies.
     *
     * @param sender  The sender where the output should be returned. If null, no output will be returned.
     * @param plugins All plugins to install. This will include plugins that the plugins depends on.
     */
    public void updatePlugins(CommandSender sender, String... plugins) {
        installPlugins(sender, false, true, plugins);
    }

    /**
     * Installs plugins without giving an output but updating exisiting ones.
     *
     * @param softDepend
     * @param plugins
     */
    public void updatePlugins(boolean softDepend, String... plugins) {
        installPlugins(null, softDepend, true, plugins);
    }

    /**
     * Install plugins without giving an output and without updating existing ones and but installs/updates soft-dependencies.
     *
     * @param plugins The plugins to install
     */
    public void updatePlugins(String... plugins) {
        installPlugins(null, false, true, plugins);
    }

    /**
     * Removes the given plugins.
     *
     * @param sender  The sender to send messages to.
     * @param plugins The names of the plugins that are to uninstall.
     */
    public void removePlugins(CommandSender sender, String... plugins) {
        MessageReceiver receiver = null;
        if (sender == null) {
            receiver = new CommandSenderReceiver(sender);
        } else {
            receiver = DEV_NULL;
        }

        Uninstaller uninstaller = new Uninstaller(receiver, install.getPluginList());
        try {
            uninstaller.uninstallPlugins(plugins);
        } catch (FailedExecutionException e) {
            receiver.sendMessage(ChatColor.RED + "Failed to uninstall plugins...");
            receiver.sendMessage(ChatColor.RED + "  See console.");
            e.printStackTrace();
        }
    }

    /**
     * Removes plugins without giving an output.
     *
     * @param plugins The plugins to install.
     */
    public void removePlugins(String... plugins) {
        removePlugins(null, plugins);
    }}
