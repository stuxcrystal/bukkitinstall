/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.stuxcrystal.bukkitinstall;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import net.stuxcrystal.bukkitinstall.api.*;
import net.stuxcrystal.bukkitinstall.commandhandler.CommandHandler;
import net.stuxcrystal.bukkitinstall.commandhandler.TranslationManager;
import net.stuxcrystal.bukkitinstall.commands.AdminCommands;
import net.stuxcrystal.bukkitinstall.commands.PluginCommands;
import net.stuxcrystal.bukkitinstall.commands.InstallCommands;
import net.stuxcrystal.bukkitinstall.commands.VanillaPluginListing;

import net.stuxcrystal.bukkitinstall.databases.BukGetDatabase;
import net.stuxcrystal.bukkitinstall.databases.URLDatabase;
import net.stuxcrystal.bukkitinstall.executors.AutoLoader;
import net.stuxcrystal.bukkitinstall.l10n.CommandMigrator;
import net.stuxcrystal.bukkitinstall.l10n.Translations;
import net.stuxcrystal.bukkitinstall.migrators.CraftBukkitUpToDateMigrator;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * BukkitInstall installs automatically plugins using<br>
 * {@code bi install <Pluginnames>}
 *
 * @author StuxCrystal
 */
public class BukkitInstallPlugin extends JavaPlugin implements BukkitInstall {

    /**
     * The instance of BukkitInstall.
     */
    private static BukkitInstallPlugin INSTANCE;

    /**
     * The command handler.
     */
    private CommandHandler handler;

    /**
     * The Plugin list.
     */
    private PluginList plugins;

    /**
     * The installation API.
     */
    private PluginController controller;

    /**
     * The Plugin-Searcher that queries the Plugin-Databases.
     */
    private PluginSearcher searcher;

    /**
     * The plugin migrator instance.
     */
    private PluginMigrator migrator;

    /**
     * The ConfigurationFile of BukkitInstall.
     */
    private ConfigurationFile configuration;

    /**
     * The translation-manager.
     */
    private Translations translator;

    /**
     * Called when the plugin is enabled.
     */
    @Override
    public void onEnable() {
        // Register instance.
        INSTANCE = this;

        // Load the configuration.
        this.saveDefaultConfig();
        this.configuration = new ConfigurationFile(
                this, new File(this.getDataFolder(), "config.yml"), this.getConfig().getDefaults()
        );

        // Register commands.
        this.handler = new CommandHandler(this);
        this.handler.registerCommands(new AdminCommands(this));
        this.handler.registerCommands(new InstallCommands(this));
        this.handler.registerCommands(new PluginCommands(this));

        // Load localization
        this.translator = new Translations(this.configuration, this);
        TranslationManager.getInstance().setHandler(new CommandMigrator());

        // Loading Plugin commands...
        Bukkit.getPluginManager().registerEvents(new VanillaPluginListing(), this);

        // Initialize API-Parts.
        this.plugins = new PluginList(this.configuration, this.getPluginLoader(), Bukkit.getPluginManager());
        this.controller = new PluginController(this);
        this.searcher = new PluginSearcher(this.configuration);
        this.migrator = new PluginMigrator();

        // Registering Plugin Databases.
        this.searcher.registerDatabase(new BukGetDatabase());
        this.searcher.registerDatabase(new URLDatabase(this));

        // Register Update-Migrators
        this.migrator.registerMigrator(new CraftBukkitUpToDateMigrator(this));

        // Load additional directories
        if (this.configuration.getFeatures().contains("autoload")) {
            this.getLogger().info("Loading additional directories...");
            AutoLoader loader = new AutoLoader(
                this.configuration, this, Bukkit.getPluginManager(), Bukkit.getScheduler()
            );
        } else {
            this.getLogger().info("AutoLoad-Feature is currently disabled.");
        }
    }

    /**
     * Called when the plugin will be disabled.
     */
    @Override
    public void onDisable() {

    }

    /**
     * Just redirect to the command handler.
     * @param sender
     * @param command
     * @param label
     * @param args
     * @return
     */
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        return this.handler.onCommand(sender, command, label, args);
    }

    /**
     * Hopefully called when a user presses the tab key.
     * @param sender
     * @param command
     * @param label
     * @param args
     * @return
     */
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        return Arrays.asList(this.getPluginList().getPluginNames());
    }

    /**
     * The CommandHandler that are processing the commands.
     *
     * @return
     */
    public CommandHandler getCommandHandler() {
        return this.handler;
    }

    @Override
    public PluginList getPluginList() {
        return this.plugins;
    }


    @Override
    public PluginController getPluginController() {
        return this.controller;
    }

    @Override
    public PluginSearcher getPluginSearcher() {
        return this.searcher;
    }

    @Override
    public PluginMigrator getPluginMigrator() {
        return this.migrator;
    }

    /**
     * Returns the configuration for the current plugin.
     * @return
     */
    public ConfigurationFile getConfiguration() {
        return this.configuration;
    }

    public Translations getTranslator() {
        return this.translator;
    }

    /**
     * The instance of BukkitInstall.
     *
     * @return
     */
    public static BukkitInstallPlugin getInstance() {
        return INSTANCE;
    }

}
