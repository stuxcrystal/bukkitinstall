/*
 * Copyright 2013 stuxcrystal
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import net.stuxcrystal.bukkitinstall.utils.MapStringFormatter;
import org.bukkit.ChatColor;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Test if the formatter works
 *
 * @author stuxcrystal
 */
public class FormatterTest {

    public static void main(String[] values) {
        Map<String, String> data = new HashMap<String, String>();
        for (ChatColor c : ChatColor.values()) {
            data.put("color:}" + c.name(), new String(new char[]{ChatColor.COLOR_CHAR, c.getChar()}));
        }

        String pre = "/{color:/}RED}TestTestTest";

        System.out.println(pre);
        System.out.println(MapStringFormatter.format(pre, data));
    }

}
